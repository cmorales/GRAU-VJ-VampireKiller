#pragma once

#include "cTexture.h"

#define SCENE_Xo		(2*TILE_SIZE)
#define SCENE_Yo		TILE_SIZE
#define SCENE_WIDTH		100
#define SCENE_HEIGHT	100

#define FILENAME		"level"
#define FILENAME_EXT	".txt"

#define TILE_SIZE		32
#define BLOCK_SIZE		32

class cScene
{
public:
	cScene(void);
	virtual ~cScene(void);

	bool LoadLevel(int level);
	void Draw(int tex_id);
	void DrawInterfaz(int hpp, int turb, double camx, double camy, int cant_throw, int lives, int stage, int points, int phase, int llave, bool supalatigo);
	void DrawDead(double camx, double camy);
	int *GetMap();
	void DrawGuarro(int tex_id, float xo, float yo, float xf, float yf, float scx, float scy, int ancho, int largo);
	void draw_number(int screen_x, int screen_y, int t, int l);

private:
	int map[SCENE_WIDTH * SCENE_HEIGHT];	//scene
	int id_DL;								//actual level display list
};
