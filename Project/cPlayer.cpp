
#include "cPlayer.h"

cPlayer::cPlayer() {}
cPlayer::~cPlayer(){}

void cPlayer::Draw(int tex_id)
{	
	float xo,yo,xf,yf;
	int st = GetState();

	switch(st)
	{
		//1
		case STATE_LOOKLEFT:				xo = 0.0f;	yo = 0.25f;
											SetWidthHeight(32, 64);
											break;
		//4
		case STATE_LOOKRIGHT:				xo = 0.05f;	yo = 0.25f;
											SetWidthHeight(32, 64);
											break;
		//1..3
		case STATE_WALKLEFT:				xo = 0.0f;	yo = 0.25f + (GetFrame()*0.25f);
											SetWidthHeight(32, 64);
											NextFrame(3);
											break;
		//4..6
		case STATE_WALKRIGHT:				xo = 0.05f; yo = 0.25f + (GetFrame()*0.25f);
											SetWidthHeight(32, 64);
											NextFrame(3);
											break;

		case STATE_ESCALERARIGHTBOT:		xo = 0.1f; yo = 0.75f + ((SubiendoAct() - 1) * 0.25f);
											NextFrame(2);
											break;

		case STATE_ESCALERALEFTBOT:			xo = 0.15f; yo = 0.25f + ((SubiendoAct() - 1) * 0.25f);
											NextFrame(2);
											break;

		case STATE_ESCALERARIGHT:			xo = 0.1f; yo = 0.25f + ((SubiendoAct() - 1) * 0.25f);
											NextFrame(2);
											break;

		case STATE_ESCALERALEFT:			xo = 0.15f; yo = 0.25f + ((SubiendoAct() - 1) * 0.25f);
											NextFrame(2);
											break;

		case STATE_WAITINGESCALERARIGHT:	xo = 0.1f; yo = 0.25f;
											break;

		case STATE_WAITINGESCALERALEFT:		xo = 0.15f; yo = 0.25f;
											break;

		case STATE_WAITINGESCALERALEFTBOT:	xo = 0.15f; yo = 0.75f;
											break;

		case STATE_WAITINGESCALERARIGHTBOT:	xo = 0.1f; yo = 0.75f;
											break;

		case STATE_THROWLEFT:				xo = 0.3f; yo = 0.25f + ((ThrowAct() - 1)*0.25f);
											NextFrame(3);
											break;

		case STATE_THROWRIGHT:				xo = 0.2f; yo = 0.25f + ((ThrowAct() - 1)*0.25f);
											NextFrame(3);
											break;
		case STATE_ATTACKLEFT:				if(AttackAct() <= 2) xo = 0.5f; 
											else if (AttackAct() == 3) xo = 0.55f;
											yo = 0.25f + ((AttackAct() - 1)*0.25f);
											NextFrame(3);
											break;
		case STATE_ATTACKRIGHT:				xo = 0.4f; yo = 0.25f + ((AttackAct() - 1)*0.25f);
											NextFrame(3);
											break;
		case STATE_CROUCHLEFT:				xo = 0.0f; yo = 1.0f;
											break;
		case STATE_CROUCHRIGHT:				xo = 0.05f; yo = 1.0f;
											break;
		case STATE_ATTACKCROUCHLEFT:		if (AttackAct() <= 2) xo = 0.8f;
											else if (AttackAct() == 3) xo = 0.85f;
											yo = 0.25f + ((AttackAct() - 1)*0.25f);
											NextFrame(3);
											break;
		case STATE_ATTACKCROUCHRIGHT:		xo = 0.7f; yo = 0.25f + ((AttackAct() - 1)*0.25f);
											NextFrame(3);
											break;
		case STATE_ATTACKESCALERALEFT:		if (AttackAct() <= 2) xo = 0.65f;
											else if (AttackAct() == 3) xo = 0.70f;
											yo = 0.25f + ((AttackAct() - 1)*0.25f);
											NextFrame(3);
											break;
		case STATE_ATTACKESCALERARIGHT:		xo = 0.55f; 
											yo = 0.25f + ((AttackAct() - 1)*0.25f);
											NextFrame(3);
											break;
		case STATE_ATTACKESCALERALEFTBOT:	if (AttackAct() <= 2) xo = 0.95f;
											else if (AttackAct() == 3) xo = 1.0f;
											yo = 0.25f + ((AttackAct() - 1)*0.25f);
											NextFrame(3);
											break;
		case STATE_ATTACKESCALERARIGHTBOT:	xo = 0.85f;
											yo = 0.25f + ((AttackAct() - 1)*0.25f);
											NextFrame(3);
											break;
		case STATE_DEAD:					xo = 0.3f;
											yo = 0.25f + ((DeadAct() - 1)*0.25f);
											break;
	}

	if (GetJump() & (st != STATE_THROWLEFT & st != STATE_THROWRIGHT & st != STATE_ATTACKLEFT & st != STATE_ATTACKRIGHT & st != STATE_ATTACKCROUCHLEFT & st != STATE_ATTACKCROUCHRIGHT)){
			yo = 1.0f;
	}
	
	xf = xo + 0.05f;
	if (st == STATE_THROWRIGHT | st == STATE_ATTACKRIGHT | st == STATE_ATTACKCROUCHRIGHT | st == STATE_ATTACKESCALERARIGHT | st == STATE_ATTACKESCALERARIGHTBOT) {
		xf = xo + 0.1f;
		NextFrame(3);
	}
	if (st == STATE_THROWLEFT | st == STATE_ATTACKLEFT | st == STATE_ATTACKCROUCHLEFT | st == STATE_ATTACKESCALERALEFT | st == STATE_ATTACKESCALERALEFTBOT) {
		xf = xo - 0.1f;
		NextFrame(3);
	}
	yf = yo - 0.25f;
	if (AttackAct() == 3) {
		if (st == STATE_ATTACKLEFT | st == STATE_ATTACKCROUCHLEFT | st == STATE_ATTACKESCALERALEFT | st == STATE_ATTACKESCALERALEFTBOT) xf = xo - 0.15f;
		else xf = xo + 0.15f;
	}
	int g = GetDamaged();
	if (g) {
		if ((g % 20) < 10) {
			xo = 0.2f; xf = 0.3f;
			yo = 1.0f; yf = 0.75f;
		}
		SetDamaged(g - 1);
	}
	if (st == STATE_DEAD) {
		xf = xo + 0.1f;
	}
	else {
		float a = GetTurbo() + 0.025f;
		if (a >= 100.0f) a = 100.0f;
		SetTurbo(a);
	}

	DrawRect(tex_id,xo,yo,xf,yf);
}
