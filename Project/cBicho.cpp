#include "cBicho.h"
#include "cScene.h"
#include "Globals.h"
#include "cData.h"

cBicho::cBicho(void)
{
	seq=0;
	delay=0;
	llaves = 0;
	jumping = false;
	subiendo = false;
	subiendo_act = 0;
	attack_act = 0;
	throw_act = 0;
	dead_act = 0;
	hp = 100;
	lives = 3;
	cantidad_throw = 10;
	supalatigo = false;
	turbo = 0.0f;
}
cBicho::~cBicho(void){}

cBicho::cBicho(int posx,int posy,int width,int height)
{
	x = posx;
	y = posy;
	w = width;
	h = height;
	seq = 0;
	delay = 0;
	llaves = 0;
	jumping = false;
	subiendo = false;
	subiendo_act = 0;
	attack_act = 0;
	throw_act = 0;
	hp = 100;
	lives = 3;
	cantidad_throw = 10;
	supalatigo = false;
	turbo = 0.0f;
}

int cBicho::GetThrowa()
{
	return cantidad_throw;
}

void cBicho::SetThrowa(int a)
{
	if (a > 99) cantidad_throw = 99;
	else cantidad_throw = a;
}

void cBicho::SetLives(int l)
{
	if (l > 9) lives = 9;
	else lives = l;
}

int cBicho::GetLives()
{
	return lives;
}

void cBicho::SetPosition(int posx,int posy)
{
	x = posx;
	y = posy;
}
void cBicho::GetPosition(int *posx,int *posy)
{
	*posx = x;
	*posy = y;
}
void cBicho::SetTile(int tx,int ty)
{
	x = tx * TILE_SIZE;
	y = ty * TILE_SIZE;
}
void cBicho::GetTile(int *tx,int *ty)
{
	*tx = x / TILE_SIZE;
	*ty = y / TILE_SIZE;
}
void cBicho::SetWidthHeight(int width,int height)
{
	w = width;
	h = height;
}
void cBicho::GetWidthHeight(int *width,int *height)
{
	*width = w;
	*height = h;
}
bool cBicho::Collides(cRect *rc)
{
	return ((x>rc->left) && (x+w<rc->right) && (y>rc->bottom) && (y+h<rc->top));
}
bool cBicho::CollidesMapEscaleraBot(int *map, bool right) {
	int tile_x, tile_y;
	int j;
	int width_tiles, height_tiles;

	tile_x = x / TILE_SIZE;
	tile_y = (y / TILE_SIZE) - 1;
	width_tiles = w / TILE_SIZE;
	height_tiles = h / TILE_SIZE;

	if (right && (state == STATE_LOOKLEFT | state == STATE_WALKLEFT))	tile_x += width_tiles;
	if (right && (state == STATE_LOOKRIGHT | state == STATE_WALKRIGHT))	tile_x += width_tiles;
	for (j = 0; j < height_tiles / 2 /*& right == false*/; j++) //izkierda
	{
		if (map[tile_x + ((tile_y + j)*SCENE_WIDTH)] == 29 | map[tile_x + ((tile_y + j)*SCENE_WIDTH)] == 30)	{
			state = STATE_WAITINGESCALERALEFTBOT;
			if (subiendo == false)  {
				if ((tile_x*TILE_SIZE) >= x) SetPosition(x - (x % TILE_SIZE) + ((TILE_SIZE*3)/2), y);
				else SetPosition(x - (x % TILE_SIZE) + (TILE_SIZE / 2), y);
				subiendo_act = 1;
			}
			subiendo = true;
			seq = 0;
			delay = 0;
			MoveEscalera(map, false);
			return true;
		}
	}
	for (j = 0; j < height_tiles / 2 /*& right == true*/; j++) //derecha
	{
		if (map[tile_x + ((tile_y + j)*SCENE_WIDTH)] == 32 | map[tile_x + ((tile_y + j)*SCENE_WIDTH)] == 31)	{
			state = STATE_WAITINGESCALERARIGHTBOT;
			if (subiendo == false)  {
				if ((tile_x*TILE_SIZE) >= x) SetPosition(x - (x % TILE_SIZE) + (TILE_SIZE / 2), y);
				else SetPosition(x - (x % TILE_SIZE) - (TILE_SIZE / 2), y);
				subiendo_act = 1;
			}
			subiendo = true;
			seq = 0;
			delay = 0;
			MoveEscalera(map, false);
			return true;
		}
	}
	return false;
}
unsigned int time_subir;
bool cBicho::CollidesMapEscalera(int *map, bool right) {
	int tile_x, tile_y;
	int j;
	int width_tiles, height_tiles;

	tile_x = x / TILE_SIZE;
	tile_y = y / TILE_SIZE;
	width_tiles = w / TILE_SIZE;
	height_tiles = h / TILE_SIZE;

	if (right && (state == STATE_LOOKLEFT | state == STATE_WALKLEFT))	tile_x += width_tiles;
	if (right && (state == STATE_LOOKRIGHT | state == STATE_WALKRIGHT))	tile_x += width_tiles;
	for (j = 0; j < height_tiles/2 & right == false; j++) //izkierda
	{
		if (map[tile_x + ((tile_y + j)*SCENE_WIDTH)] == 32 | map[tile_x + ((tile_y + j)*SCENE_WIDTH)] == 31)	{
			state = STATE_WAITINGESCALERALEFT;
			time_subir = glutGet(GLUT_ELAPSED_TIME);
			if (subiendo == false)  {
				SetPosition(x - (x % TILE_SIZE) + (TILE_SIZE / 2), y);
				subiendo_act = 1;
				time_subir -= delay_subir;
			}
			subiendo = true;
			seq = 0;
			delay = 0;
			MoveEscalera(map, true);
			return true;
		}
	}
	for (j = 0; j < height_tiles/2 & right == true; j++) //derecha
	{
		if (map[tile_x + ((tile_y + j)*SCENE_WIDTH)] == 29 | map[tile_x + ((tile_y + j)*SCENE_WIDTH)] == 30)	{
			state = STATE_WAITINGESCALERARIGHT;
			time_subir = glutGet(GLUT_ELAPSED_TIME);
			if (subiendo == false)  {
				SetPosition(x - (x % TILE_SIZE) + (TILE_SIZE / 2), y);
				subiendo_act = 1;
				time_subir -= delay_subir;
			}
			subiendo = true;
			seq = 0;
			delay = 0;
			MoveEscalera(map, true);
			return true;
		}
	}
	return false;
}
int cBicho::SubiendoAct() {
	return subiendo_act;
}
bool cBicho::CollidesMapWall(int *map,bool right)
{
	int tile_x,tile_y;
	int j, i;
	int width_tiles,height_tiles;

	tile_x = x / TILE_SIZE;
	tile_y = y / TILE_SIZE;
	width_tiles  = w / TILE_SIZE;
	height_tiles = h / TILE_SIZE;

	if (right) {
		if (state == STATE_ATTACKRIGHT & attack_act == 3) tile_x += 1;
		else if (state == STATE_ATTACKLEFT & attack_act <= 2) tile_x += 1;
		else if (state == STATE_THROWRIGHT & throw_act == 3) tile_x += 1;
		else if (state == STATE_THROWLEFT & throw_act <= 2) tile_x += 1;
		else tile_x += width_tiles;
	}
	else if (state == STATE_THROWRIGHT & throw_act <= 2) tile_x += 1;
	else if (state == STATE_THROWLEFT & throw_act == 3) tile_x += 1;
	else if (state == STATE_THROWLEFT | state == STATE_THROWRIGHT);
	else if (state == STATE_ATTACKRIGHT & attack_act <= 2) tile_x += 1;
	else if (state == STATE_ATTACKLEFT & attack_act == 3) tile_x += 2;
	//if (attack_act == 3 && right) tile_x -= 2;
	//if (attack_act == 3 && state == STATE_ATTACKLEFT && right) tile_x += width_tiles;
	//if (attack_act == 3 && right) tile_x += 1;
	
	for(j=0;j<height_tiles;j++)
	{
		if (map[(tile_x) + ((tile_y + j)*SCENE_WIDTH)] == 1) return true;
	}
	return false;
}

#define slowsubir 0.1
#define subirp 16
void cBicho::MoveEscalera(int *map, bool right)
{
	if (glutGet(GLUT_ELAPSED_TIME) > (time_subir + delay_subir)) {
		time_subir = glutGet(GLUT_ELAPSED_TIME);
		if (subiendo_act <= 1) {
			subiendo_act += 1;
			if (right) { //subir
				switch (state) {
				case STATE_WAITINGESCALERARIGHT: //1
					x += subirp;
					y += subirp;
					state = STATE_ESCALERARIGHT;
					break;
				case STATE_WAITINGESCALERALEFT: //2
					x -= subirp;
					y += subirp;
					state = STATE_ESCALERALEFT;
					break;
				case STATE_WAITINGESCALERALEFTBOT: //3
					x += subirp;
					y += subirp;
					state = STATE_ESCALERARIGHT;
					break;
				case STATE_WAITINGESCALERARIGHTBOT: //4
					x -= subirp;
					y += subirp;
					state = STATE_ESCALERALEFT;
					break;
				case STATE_ESCALERARIGHT: //5
					x += subirp;
					y += subirp;
					break;
				case STATE_ESCALERALEFT: //6 
					x -= subirp;
					y += subirp;
					break;
				case STATE_ESCALERALEFTBOT: //7
					x += subirp;
					y += subirp;
					state = STATE_ESCALERARIGHT;
					break;
				case STATE_ESCALERARIGHTBOT: //7
					x -= subirp;
					y += subirp;
					state = STATE_ESCALERALEFT;
					break;
				}
			}
			else { //bajar
				switch (state) {
				case STATE_ESCALERARIGHT: //1
					x -= subirp;
					y -= subirp;
					state = STATE_ESCALERALEFTBOT;
					break;
				case STATE_ESCALERALEFT: //2
					x += subirp;
					y -= subirp;
					state = STATE_ESCALERARIGHTBOT;
					break;
				case STATE_ESCALERARIGHTBOT: //3
					x += subirp;
					y -= subirp;
					break;
				case STATE_ESCALERALEFTBOT: //4
					x -= subirp;
					y -= subirp;
					break;
				case STATE_WAITINGESCALERARIGHT: //5
					x -= subirp;
					y -= subirp;
					state = STATE_ESCALERALEFTBOT;
					break;
				case STATE_WAITINGESCALERALEFTBOT: //6
					x -= subirp;
					y -= subirp;
					state = STATE_ESCALERALEFTBOT;
					break;
				case STATE_WAITINGESCALERARIGHTBOT: //7
					x += subirp;
					y -= subirp;
					state = STATE_ESCALERARIGHTBOT;
					break;
				case STATE_WAITINGESCALERALEFT: //8
					x += subirp;
					y -= subirp;
					state = STATE_ESCALERARIGHTBOT;
					break;
				}
			}
			//int i = glutGet(GLUT_ELAPSED_TIME);
			//while ((i + 100) > glutGet(GLUT_ELAPSED_TIME));
		}
		else {
			subiendo_act = 1;
			if (FinEscalera(map) | CollidesMapFloor(map)) {
				if (state == STATE_ESCALERALEFT | state == STATE_ESCALERALEFTBOT) state = STATE_LOOKLEFT;
				else state = STATE_LOOKRIGHT;
				subiendo = 0;
				subiendo_act = 0;
			}
			else if (state == STATE_ESCALERALEFT) state = STATE_WAITINGESCALERALEFT;
			else if (state == STATE_ESCALERALEFTBOT) state = STATE_WAITINGESCALERALEFTBOT;
			else if (state == STATE_ESCALERARIGHTBOT) state = STATE_WAITINGESCALERARIGHTBOT;
			else if (state == STATE_ESCALERARIGHT) state = STATE_WAITINGESCALERARIGHT;
		}
	}
}

bool cBicho::FinEscalera(int *map)
{
	int tile_x, tile_y;
	int j;
	int width_tiles, height_tiles;

	tile_x = x / TILE_SIZE;
	tile_y = y / TILE_SIZE;
	width_tiles = w / TILE_SIZE;
	height_tiles = h / TILE_SIZE;

	for (j = 0; j < height_tiles; j++)
	{
		if (map[tile_x + ((tile_y - 1 + j)*SCENE_WIDTH)] == 32)	return false;
		if (map[tile_x + ((tile_y - 1 + j)*SCENE_WIDTH)] == 31)	return false;
	}
	//tile_x += width_tiles;
	for (j = 0; j < height_tiles; j++)
	{
		if (map[tile_x + ((tile_y - 1 + j)*SCENE_WIDTH)] == 30)	return false;
		if (map[tile_x + ((tile_y - 1 + j)*SCENE_WIDTH)] == 29)	return false;
	}
	return true;
}

int cBicho::ThrowAct() {
	return throw_act;
}

int cBicho::GetHp() {
	return hp;
}

bool cBicho::CollidesMapFloor(int *map)
{
	int tile_x,tile_y;
	int width_tiles;
	bool on_base;
	int i;

	tile_x = x / TILE_SIZE;
	tile_y = y / TILE_SIZE;

	width_tiles = w / TILE_SIZE;
	if (attack_act == 1 | attack_act == 2 | throw_act == 1 | throw_act == 2){
		if (state == STATE_ATTACKRIGHT | state == STATE_ATTACKCROUCHRIGHT | state == STATE_THROWRIGHT) tile_x += 1;
		width_tiles = 1;
	}
	if (attack_act == 3 | throw_act == 3) {
		if(state == STATE_ATTACKLEFT | state == STATE_ATTACKCROUCHLEFT) tile_x += 2;
		if (state == STATE_THROWLEFT) tile_x += 1;
		width_tiles = 1;
	}
	if ((x % TILE_SIZE) != 0) width_tiles++;

	on_base = false;
	i=0;
	while((i<width_tiles) && !on_base)
	{
		if( (y % TILE_SIZE) == 0 )
		{
			if (map[(tile_x + i) + ((tile_y - 1) * SCENE_WIDTH)] == 1)
				on_base = true;
			/*else if (map[(tile_x + i) + ((tile_y - 1) * SCENE_WIDTH)] == 19)
				on_base = true;
			else if (map[(tile_x + i) + ((tile_y - 1) * SCENE_WIDTH)] == 20)
				on_base = true;*/
		}
		else
		{
			if(map[ (tile_x + i) + (tile_y * SCENE_WIDTH) ] == 1)
			{
				y = (tile_y + 1) * TILE_SIZE;
				on_base = true;
			}
			/*else if (map[(tile_x + i) + (tile_y * SCENE_WIDTH)] == 19)
			{
				y = (tile_y + 1) * TILE_SIZE;
				on_base = true;
			}
			else if (map[(tile_x + i) + (tile_y * SCENE_WIDTH)] == 20)
			{
				y = (tile_y + 1) * TILE_SIZE;
				on_base = true;
			}*/
		}
		i++;
	}
	return on_base;
}

void cBicho::GetArea(cRect *rc)
{
	rc->left   = x;
	rc->right  = x+w;
	rc->bottom = y;
	rc->top    = y+h;
}

void cBicho::DrawRect(int tex_id,float xo,float yo,float xf,float yf)
{
	int screen_x,screen_y;

	screen_x = x + SCENE_Xo;
	screen_y = y + SCENE_Yo + (BLOCK_SIZE - TILE_SIZE);

	glEnable(GL_TEXTURE_2D);
	
	glBindTexture(GL_TEXTURE_2D, tex_id);
	glBegin(GL_QUADS);	
		glTexCoord2f(xo,yo);	glVertex2i(screen_x  ,screen_y);
		glTexCoord2f(xf,yo);	glVertex2i(screen_x+w,screen_y);
		glTexCoord2f(xf,yf);	glVertex2i(screen_x+w,screen_y+h);
		glTexCoord2f(xo,yf);	glVertex2i(screen_x  ,screen_y+h);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

void cBicho::MoveLeft(int *map)
{
	int xaux;
	
	//Whats next tile?
	if((x % TILE_SIZE) == 0)
	{
		xaux = x;
		x -= STEP_LENGTH;

		if(CollidesMapWall(map,false))
		{
			x = xaux;
			if (state != STATE_ATTACKRIGHT & state != STATE_ATTACKLEFT & state != STATE_THROWLEFT & state != STATE_THROWRIGHT) state = STATE_LOOKLEFT;
		}
	}
	//Advance, no problem
	else
	{
		
		if (state != STATE_WALKLEFT & state != STATE_ATTACKLEFT & state != STATE_ATTACKRIGHT & state != STATE_THROWLEFT & state != STATE_THROWRIGHT)
		{
			state = STATE_WALKLEFT;
			seq = 0;
			delay = 0;
		}
		x -= STEP_LENGTH;
	}
}

unsigned int time_act_attack;
void cBicho::Attack(bool right) {
	if (right) {
		if (state != STATE_ATTACKRIGHT & state != STATE_ATTACKCROUCHRIGHT & state != STATE_ATTACKESCALERARIGHT & state != STATE_ATTACKESCALERARIGHTBOT & attack_act <= 1)
		{
			if (state == STATE_CROUCHRIGHT) {
				state = STATE_ATTACKCROUCHRIGHT;
				seq = 0;
				delay = 0;
				attack_act = 1;
				time_act_attack = glutGet(GLUT_ELAPSED_TIME);
			}
			else if (state == STATE_WAITINGESCALERARIGHT) {
				state = STATE_ATTACKESCALERARIGHT;
				seq = 0;
				delay = 0;
				attack_act = 1;
				time_act_attack = glutGet(GLUT_ELAPSED_TIME);
			}
			else if (state == STATE_WAITINGESCALERARIGHTBOT) {
				state = STATE_ATTACKESCALERARIGHTBOT;
				seq = 0;
				delay = 0;
				attack_act = 1;
				time_act_attack = glutGet(GLUT_ELAPSED_TIME);
			}
			else{
				state = STATE_ATTACKRIGHT;
				seq = 0;
				delay = 0;
				attack_act = 1;
				time_act_attack = glutGet(GLUT_ELAPSED_TIME);
			}


		}
	}
	else {
		if (state != STATE_ATTACKLEFT & state != STATE_ATTACKCROUCHLEFT & state != STATE_ATTACKESCALERALEFT & state != STATE_ATTACKESCALERALEFTBOT & attack_act <= 1)
		{
			if (state == STATE_CROUCHLEFT){
				state = STATE_ATTACKCROUCHLEFT;
				seq = 0;
				delay = 0;
				attack_act = 1;
				time_act_attack = glutGet(GLUT_ELAPSED_TIME);
			}
			else if (state == STATE_WAITINGESCALERALEFT){
				state = STATE_ATTACKESCALERALEFT;
				seq = 0;
				delay = 0;
				attack_act = 1;
				time_act_attack = glutGet(GLUT_ELAPSED_TIME);
			}
			else if (state == STATE_WAITINGESCALERALEFTBOT){
				state = STATE_ATTACKESCALERALEFTBOT;
				seq = 0;
				delay = 0;
				attack_act = 1;
				time_act_attack = glutGet(GLUT_ELAPSED_TIME);
			}
			else {
				state = STATE_ATTACKLEFT;
				seq = 0;
				delay = 0;
				attack_act = 1;
				time_act_attack = glutGet(GLUT_ELAPSED_TIME);
			}
		}

	}
	if (glutGet(GLUT_ELAPSED_TIME) > (time_act_attack + delay_attack)) {
		attack_act += 1;
		time_act_attack = glutGet(GLUT_ELAPSED_TIME);
	}
}
int cBicho::AttackAct()
{
	return attack_act;
}

void cBicho::SetAttackAct(int a) {
	attack_act = a;
}

unsigned int time_act_throw;
bool cBicho::Throw(bool right) {
	if (cantidad_throw <= 0) return false;
	if (right) {
		if (state != STATE_THROWRIGHT)
		{
			state = STATE_THROWRIGHT;
			seq = 0;
			delay = 0;
			throw_act = 1;
			time_act_throw = glutGet(GLUT_ELAPSED_TIME);
		}
	}
	else {
		if (state != STATE_THROWLEFT)
		{
			state = STATE_THROWLEFT;
			seq = 0;
			delay = 0;
			throw_act = 1;
			time_act_throw = glutGet(GLUT_ELAPSED_TIME);
		}
	}
	if (glutGet(GLUT_ELAPSED_TIME) > (time_act_throw + delay_throw)) {
		throw_act += 1;
		time_act_throw = glutGet(GLUT_ELAPSED_TIME);
	}
	if (throw_act >= 4) {
		cantidad_throw -= 1;
	}
	return true;
}
void cBicho::SetThrowAct(int a){
	throw_act = a;
}
void cBicho::MoveRight(int *map)
{
	int xaux = x;

	//Whats next tile?
	if( (x % TILE_SIZE) == 0)
	{
		xaux = x;
		x += STEP_LENGTH;

		if (CollidesMapWall(map, true))
		{
			x = xaux;
			if (state != STATE_ATTACKRIGHT & state != STATE_ATTACKLEFT & state != STATE_THROWLEFT & state != STATE_THROWRIGHT) state = STATE_LOOKRIGHT;
		}
	}
	//Advance, no problem
	else
	{

		if (state != STATE_WALKRIGHT & state != STATE_ATTACKRIGHT & state != STATE_ATTACKLEFT & state != STATE_THROWLEFT & state != STATE_THROWRIGHT)
		{
			state = STATE_WALKRIGHT;
			seq = 0;
			delay = 0;
		}
		/*else if(state == STATE_ATTACKRIGHT) {
			x += TILE_SIZE;
			if (CollidesMapWall(map, true)) x = xaux - STEP_LENGTH;
			else x = xaux;
		}*/
		x += STEP_LENGTH;
	}
}
void cBicho::Stop()
{
	switch(state)
	{
		case STATE_WALKLEFT:	state = STATE_LOOKLEFT;			break;
		case STATE_WALKRIGHT:	state = STATE_LOOKRIGHT;		break;
		case STATE_ESCALERALEFT: state = STATE_ESCALERALEFT;		break;
		case STATE_ESCALERARIGHT: state = STATE_ESCALERARIGHT;		break;
		case STATE_ESCALERALEFTBOT: state = STATE_ESCALERALEFTBOT;		break;
		case STATE_ESCALERARIGHTBOT: state = STATE_ESCALERARIGHTBOT;		break;
		case STATE_CROUCHLEFT: state = STATE_LOOKLEFT; break;
		case STATE_CROUCHRIGHT: state = STATE_LOOKRIGHT; break;
		case STATE_ATTACKESCALERARIGHT: state = STATE_ATTACKESCALERARIGHT; break;
		case STATE_ATTACKESCALERALEFT: state = STATE_ATTACKESCALERALEFT; break;
	}
}
void cBicho::Jump(int *map)
{
	if(!jumping)
	{
		if(CollidesMapFloor(map))
		{
			jumping = true;
			jump_alfa = 0;
			jump_y = y;
		}
	}
}
void cBicho::Logic(int *map)
{
	float alfa;

	if(jumping)
	{
		jump_alfa += JUMP_STEP;
		
		if(jump_alfa == 180)
		{
			jumping = false;
			y = jump_y;
		}
		else
		{
			alfa = ((float)jump_alfa) * 0.017453f;
			y = jump_y + (int)( ((float)JUMP_HEIGHT) * sin(alfa) );
		
			if(jump_alfa > 90)
			{
				//Over floor?
				jumping = !CollidesMapFloor(map);
			}
		}
	}
	else if (subiendo)
	{

	}
	else
	{
		//Over floor?
		if (!CollidesMapFloor(map)) {
			y -= (2 * STEP_LENGTH);
			
		}
		
	}
}
void cBicho::NextFrame(int max)
{
	delay++;
	if(delay == FRAME_DELAY)
	{
		seq++;
		seq%=max;
		delay = 0;
	}
}
int cBicho::GetFrame()
{
	return seq;
}
int cBicho::GetState()
{
	return state;
}

bool cBicho::GetJump()
{
	return jumping;
}

void cBicho::SetState(int s)
{
	state = s;
}

bool cBicho::GetEscalera()
{
	return subiendo;
}

void cBicho::SetSubiendo(bool b) {
	subiendo = b;
}

int cBicho::GetDamaged()
{ 
	return damaged;
}

void cBicho::SetDamaged(int a)
{
	damaged = a;
}

void cBicho::SetHp(int a)
{
	hp = a;
	if (hp <= 0) hp = -20;
	if (hp > 100) hp = 100;
}

int cBicho::GetLlaves(){
	return llaves;
}

void cBicho::SetLlaves(int l)
{
	if ((l <= 3) && (l >= 0)) llaves = l;
}

int cBicho::GetDa�oLatigo()
{
	if (supalatigo) return 2 * da�o_latigo;
	else return da�o_latigo;
}

int cBicho::GetDa�oEnergy()
{
	return da�o_energy;
}

int cBicho::DeadAct()
{
	return dead_act;
}

void cBicho::SetDeadAct(int s)
{
	dead_act = s;
}

void cBicho::SetJumping(bool a)
{
	jumping = a;
}

bool cBicho::GetSupalatigo()
{
	return supalatigo;
}

void cBicho::SetSupalatigo(bool v)
{
	supalatigo = v;
}

float cBicho::GetTurbo()
{
	return turbo;
}

void cBicho::SetTurbo(float f)
{
	turbo = f;
}
