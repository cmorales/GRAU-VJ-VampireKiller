
#include "cEnemigo.h"
//#include "cBicho.h"
#include "cPlayer.h"
#include "cGame.h"
#include <ctime>

cEnemigo::cEnemigo() 
{
	Sonido.Load();
	index_gea = 0;
}
cEnemigo::~cEnemigo(){}

void cEnemigo::AddEnemigo(int px, int py, int ptype, int respawn, bool pright) {
	for (index_enemigo = 0; index_enemigo < size_enemigos; index_enemigo++) {
		if (lista_enemigos[index_enemigo].active == false && lista_enemigos[index_enemigo].respawn_time <= 0){
			lista_enemigos[index_enemigo].x = px; lista_enemigos[index_enemigo].xo = px;
			lista_enemigos[index_enemigo].y = py; lista_enemigos[index_enemigo].yo = py;
			lista_enemigos[index_enemigo].right = pright;
			lista_enemigos[index_enemigo].active = true;
			//lista_enemigos[index_enemigo].largo = largo;
			//lista_enemigos[index_enemigo].ancho = ancho;
			//lista_enemigos[index_enemigo].largoo = largo;
			//lista_enemigos[index_enemigo].anchoo = ancho;
			lista_enemigos[index_enemigo].type = ptype;
			lista_enemigos[index_enemigo].mov_count = 0;
			lista_enemigos[index_enemigo].mov_count_att = 0;
			lista_enemigos[index_enemigo].respawn_time = respawn * 1000;
			if (ptype == Enemy_zubat) {
				lista_enemigos[index_enemigo].mov_count = 240;
				lista_enemigos[index_enemigo].hp = hp_zubat;
				lista_enemigos[index_enemigo].largo = 32;
				lista_enemigos[index_enemigo].ancho = 32;
				lista_enemigos[index_enemigo].largoo = 32;
				lista_enemigos[index_enemigo].anchoo = 32;
			}
			else if (ptype == Enemy_Ironman) {
				lista_enemigos[index_enemigo].hp = hp_ironman;
				lista_enemigos[index_enemigo].largo = 64;
				lista_enemigos[index_enemigo].ancho = 32;
				lista_enemigos[index_enemigo].largoo = 64;
				lista_enemigos[index_enemigo].anchoo = 32;
			}
			else if (ptype == Enemy_Gohan) {
				lista_enemigos[index_enemigo].hp = hp_gohan;
				lista_enemigos[index_enemigo].largo = 64;
				lista_enemigos[index_enemigo].ancho = 32;
				lista_enemigos[index_enemigo].largoo = 64;
				lista_enemigos[index_enemigo].anchoo = 32;
			}
			lista_enemigos[index_enemigo].last_action = glutGet(GLUT_ELAPSED_TIME);
			lista_enemigos[index_enemigo].state = STATE_WALK1;
			return;
		}
	}
}

void cEnemigo::CollidesAttack(int x, int y, int w, int h, int d)
{
	int i, j;
	bool c;
	bool c2;
	for (i = 0; i < size_enemigos; i++) {
		if (lista_enemigos[i].active){
			c = false;
			c2 = false;
			if (w >= lista_enemigos[i].ancho) { //L�tigo m�s ancho
				if (((lista_enemigos[i].x + lista_enemigos[i].ancho) >= x) & ((lista_enemigos[i].x + lista_enemigos[i].ancho) <= (x + w))) c = true;
				else if ((lista_enemigos[i].x >= x) & ((lista_enemigos[i].x) <= (x + w))) c = true;
			}
			else { //L�tigo m�s estrecho
				if (((x + w) > lista_enemigos[i].x) & ((x + w) < (lista_enemigos[i].x + lista_enemigos[i].ancho))) c = true;
				else if ((x > lista_enemigos[i].x) & (x < (lista_enemigos[i].x + lista_enemigos[i].ancho))) c = true;
			}
			if (c) { //si colisiona con X
				if (h >= lista_enemigos[i].largo) { //L�tigo m�s largo
					if (((lista_enemigos[i].y + lista_enemigos[i].largo) >= y) & ((lista_enemigos[i].y + lista_enemigos[i].largo) <= (y + h))) c2 = true;
					else if ((lista_enemigos[i].y >= y) & ((lista_enemigos[i].y) <= (y + h))) c2 = true;
				}
				else { //L�tigo m�s corto
					if (((y + h) > lista_enemigos[i].y) & ((y + h) < (lista_enemigos[i].y + lista_enemigos[i].largo))) c2 = true;
					else if ((y > lista_enemigos[i].y) & (y < (lista_enemigos[i].y + lista_enemigos[i].largo))) c2 = true;
				}
			}
			if (c2) {
				lista_enemigos[i].hp -= d;
				if (lista_enemigos[i].hp <= 0) {
					//Sonido.Play(SOUND_KILL);
					lista_enemigos[i].active = false;
					lista_enemigos[i].count_respawn = glutGet(GLUT_ELAPSED_TIME);
				}
				
			}
		}
	}
}

void cEnemigo::IA_Gohan(int id, int *map, int xmia, int ymia) {
	int tilex, tiley, st;
	bool canmover = false;
	bool canmovel = false;
	st = lista_enemigos[id].state;
	if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].last_action + (1000 / (60 / 2)))) {
		lista_enemigos[id].last_action = glutGet(GLUT_ELAPSED_TIME);
		if (st != STATE_ATTACK1 & st != STATE_ATTACK2 & st != STATE_ATTACK3 & st != STATE_ATTACK4) {
			tilex = ((lista_enemigos[id].x + 2) / TILE_SIZE) + (32 / TILE_SIZE);
			tiley = (lista_enemigos[id].y / TILE_SIZE) - 1;
			if ((map[tilex + (tiley*SCENE_WIDTH)] == 1) & (map[tilex + ((tiley + 1)*SCENE_WIDTH)] != 1) & (map[tilex + ((tiley + 2)*SCENE_WIDTH)] != 1)) canmover = true;
			tilex = ((lista_enemigos[id].x - 2) / TILE_SIZE);
			tiley = (lista_enemigos[id].y / TILE_SIZE) - 1;
			if ((map[tilex + (tiley*SCENE_WIDTH)] == 1) & (map[tilex + ((tiley + 1)*SCENE_WIDTH)] != 1) & (map[tilex + ((tiley + 2)*SCENE_WIDTH)] != 1)) canmovel = true;

			if (glutGet(GLUT_ELAPSED_TIME) >= (lista_enemigos[id].mov_count + time_kame_gohan) & ((ymia >= (lista_enemigos[id].y - 32)) & (ymia <= (lista_enemigos[id].y + lista_enemigos[id].largo + 32)))) { //can attack
				if (abs(lista_enemigos[id].x - xmia) <= (12 * TILE_SIZE)) { //enemy detected, move
					if (abs(lista_enemigos[id].x - xmia) <= (4 * TILE_SIZE)) { //enemy near, kame
						if (xmia < lista_enemigos[id].x) lista_enemigos[id].right = false;
						else lista_enemigos[id].right = true;
						lista_enemigos[id].state = STATE_ATTACK1;
						lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
						lista_enemigos[id].mov_count = glutGet(GLUT_ELAPSED_TIME);
						lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
						return;
					}
					else {//enemy far, move
						if ((xmia < lista_enemigos[id].x) & canmovel) { //heroe a la izk
							lista_enemigos[id].right = false;
							lista_enemigos[id].x -= 2;
							lista_enemigos[id].state = STATE_WALK4;
							lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
						}
						else if ((xmia > lista_enemigos[id].x) & canmover) { //heroe a al drx
							lista_enemigos[id].right = true;
							lista_enemigos[id].x += 2;
							lista_enemigos[id].state = STATE_WALK4;
							lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
						}
					}
				}
				else { //enemy not detected, stay
					if (lista_enemigos[id].state == STATE_WALK4) lista_enemigos[id].state = STATE_WALK3;
				}
			}
			else { //cant attack, evade
				if (abs(lista_enemigos[id].x - xmia) <= (4*TILE_SIZE)) { //heroe too cerca, evade
					if ((xmia < lista_enemigos[id].x) & canmover) { //heroe a la izk
						lista_enemigos[id].right = true;
						lista_enemigos[id].x += 2;
					}
					else if ((xmia >= lista_enemigos[id].x) & canmovel) { //heroe a al drx
						lista_enemigos[id].right = false;
						lista_enemigos[id].x -= 2;
					}
					lista_enemigos[id].state = STATE_WALK4;
				}
				else { //heroe enough lejos, stay
					if (lista_enemigos[id].state == STATE_WALK4) lista_enemigos[id].state = STATE_WALK3;
				}
			}
		}
		switch (lista_enemigos[id].state)
		{
		case STATE_WALK1:
			if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count_att + time_action_gohan)) {
				lista_enemigos[id].ancho = lista_enemigos[id].anchoo * 2;
				lista_enemigos[id].state = STATE_WALK2;
				lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
			}
			break;
		case STATE_WALK2:
			if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count_att + time_action_gohan)) {
				lista_enemigos[id].ancho = lista_enemigos[id].anchoo * 2;
				lista_enemigos[id].state = STATE_WALK3;
				lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
			}
			break;
		case STATE_WALK3:
			if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count_att + time_action_gohan)) {
				lista_enemigos[id].ancho = lista_enemigos[id].anchoo * 2;
				lista_enemigos[id].state = STATE_WALK1;
				lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
			}
			break;
		case STATE_WALK4:
			if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count_att + time_action_gohan)) {
				lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
				lista_enemigos[id].state = STATE_WALK4;
				lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
			}
			break;
		case STATE_ATTACK1:
			if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count_att + (time_action_gohan * 10))) {
				lista_enemigos[id].state = STATE_ATTACK2;
				lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
				lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
				
			}
			break;
		case STATE_ATTACK2:
			if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count_att + (time_action_gohan * 10))) {
				lista_enemigos[id].state = STATE_ATTACK3;
				Sonido.Play(SOUND_KAME);
				if (!lista_enemigos[id].right) lista_enemigos[id].x -= (5 * TILE_SIZE);
				lista_enemigos[id].ancho = lista_enemigos[id].anchoo * 6;
				lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
			}
			break;
		case STATE_ATTACK3:
			if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count_att + (time_action_gohan * 5))) {
				lista_enemigos[id].state = STATE_ATTACK4;
				lista_enemigos[id].ancho = lista_enemigos[id].anchoo * 6;
				lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
			}
			break;
		case STATE_ATTACK4:
			if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count_att + (time_action_gohan * 5))) {
				
				if (!lista_enemigos[id].right) lista_enemigos[id].x += (5 * TILE_SIZE);
				lista_enemigos[id].ancho = lista_enemigos[id].anchoo * 2;
				lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
				if (abs(lista_enemigos[id].x - xmia) <= (4 * TILE_SIZE)) {
					lista_enemigos[id].state = STATE_WALK4;
					lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
					if (xmia < lista_enemigos[id].x) lista_enemigos[id].right = false;
					else lista_enemigos[id].right = true;
				}
				else lista_enemigos[id].state = STATE_WALK1;
			}
			break;
		}
	}
}

void cEnemigo::IA_Zubat(int id) {
	if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].last_action + (1000 / (60 / 2)))) {
		lista_enemigos[id].last_action = glutGet(GLUT_ELAPSED_TIME);
		if (lista_enemigos[id].mov_count > 0) {
			if (lista_enemigos[id].yprev < lista_enemigos[id].y) { //parriba
				lista_enemigos[id].yprev = lista_enemigos[id].y;
				lista_enemigos[id].y += 2;
				if ((lista_enemigos[id].y - lista_enemigos[id].yo) > 32) lista_enemigos[id].y -= 4;
			}
			else { //pabajo
				lista_enemigos[id].yprev = lista_enemigos[id].y;
				lista_enemigos[id].y -= 2;
				if ((lista_enemigos[id].yo - lista_enemigos[id].y) > 32) lista_enemigos[id].y += 4;
			}
			if (lista_enemigos[id].right) lista_enemigos[id].x += 2;
			else lista_enemigos[id].x -= 2;
			lista_enemigos[id].mov_count -= 1;
		}
		else {
			lista_enemigos[id].active = false;
			lista_enemigos[id].count_respawn = glutGet(GLUT_ELAPSED_TIME);
		}
		switch (lista_enemigos[id].state)
		{
		case STATE_WALK1:
			if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count_att + time_action_zubat)) {
				lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
				lista_enemigos[id].state = STATE_WALK2;
			}
			break;
		case STATE_WALK2:
			if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count_att + time_action_zubat)) {
				lista_enemigos[id].state = STATE_WALK3;
				lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
			}
			break;
		case STATE_WALK3:
			if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count_att + time_action_zubat)) {
				lista_enemigos[id].state = STATE_WALK4;
				lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
			}
			break;
		case STATE_WALK4:
			if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count_att + time_action_zubat)) {
				lista_enemigos[id].state = STATE_WALK1;
				lista_enemigos[id].mov_count_att = glutGet(GLUT_ELAPSED_TIME);
			}
			break;
		}
	}
}

void cEnemigo::IA_ironman(int id, int *map, int xmia, int ymia) {
	if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].last_action + (1000/(60/2)))) {
		lista_enemigos[id].last_action = glutGet(GLUT_ELAPSED_TIME);
		int tilex, tiley, st;
		bool atck = false;
		st = lista_enemigos[id].state;
		if (st != STATE_ATTACK1 & st != STATE_ATTACK2 & st != STATE_ATTACK3 & st != STATE_ATTACK4) {
			if (lista_enemigos[id].right){
				tilex = ((lista_enemigos[id].x + 2) / TILE_SIZE) + (lista_enemigos[id].ancho / TILE_SIZE);
				tiley = (lista_enemigos[id].y / TILE_SIZE) - 1;
				if ((xmia >= lista_enemigos[id].x) & ((xmia - lista_enemigos[id].x) <= (5*TILE_SIZE))) { //attack
					if ((ymia >= lista_enemigos[id].y) & (ymia <= (lista_enemigos[id].y + lista_enemigos[id].largo))) {
						lista_enemigos[id].state = STATE_ATTACK1;
						lista_enemigos[id].ancho += ((lista_enemigos[id].ancho / 3) * 2);
						lista_enemigos[id].xprev = lista_enemigos[id].x;
						atck = true;
						
						lista_enemigos[id].mov_count = glutGet(GLUT_ELAPSED_TIME);
						return;
					}
					else if (((ymia + 64) >= lista_enemigos[id].y) & ((ymia + 64) <= (lista_enemigos[id].y + lista_enemigos[id].largoo))) {
						lista_enemigos[id].state = STATE_ATTACK1;
						lista_enemigos[id].ancho += ((lista_enemigos[id].ancho / 3) * 2);
						lista_enemigos[id].xprev = lista_enemigos[id].x;
						atck = true;
						lista_enemigos[id].mov_count = glutGet(GLUT_ELAPSED_TIME);
						return;
					}
				}
				if(!atck) { //move
					if ((map[tilex + (tiley*SCENE_WIDTH)] == 1) & (map[tilex + ((tiley + 1)*SCENE_WIDTH)] != 1) & (map[tilex + ((tiley + 2)*SCENE_WIDTH)] != 1)) {
						lista_enemigos[id].x += 2;
					}
					else {
						lista_enemigos[id].right = !lista_enemigos[id].right;
						lista_enemigos[id].state = STATE_WALK4;
						lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
					}
				}
			}
			else {
				tilex = ((lista_enemigos[id].x - 2) / TILE_SIZE);
				tiley = (lista_enemigos[id].y / TILE_SIZE) - 1;
				if (xmia <= lista_enemigos[id].x & ((lista_enemigos[id].x - xmia) <= (5 * TILE_SIZE))) { //attack
					if ((ymia >= lista_enemigos[id].y) & (ymia <= (lista_enemigos[id].y + lista_enemigos[id].largo))) {
						lista_enemigos[id].state = STATE_ATTACK1;
						lista_enemigos[id].ancho += ((lista_enemigos[id].ancho / 3) * 2);
						lista_enemigos[id].xprev = lista_enemigos[id].x;
						atck = true;
						lista_enemigos[id].mov_count = glutGet(GLUT_ELAPSED_TIME);
						return;
					}
					else if (((ymia + 64) >= lista_enemigos[id].y) & ((ymia + 64) <= (lista_enemigos[id].y + lista_enemigos[id].largoo))) {
						lista_enemigos[id].state = STATE_ATTACK1;
						lista_enemigos[id].ancho += ((lista_enemigos[id].ancho / 3) * 2);
						lista_enemigos[id].xprev = lista_enemigos[id].x;
						atck = true;
						lista_enemigos[id].mov_count = glutGet(GLUT_ELAPSED_TIME);
						return;
					}
				}
				if(!atck) { //move
					if ((map[tilex + (tiley*SCENE_WIDTH)] == 1) & (map[tilex + ((tiley + 1)*SCENE_WIDTH)] != 1) & (map[tilex + ((tiley + 2)*SCENE_WIDTH)] != 1)) {
						lista_enemigos[id].x -= 2;
					}
					else {
						lista_enemigos[id].right = !lista_enemigos[id].right;
						lista_enemigos[id].state = STATE_WALK4;
						lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
					}
				}
			}
		}
		switch (lista_enemigos[id].state)
			{
			case STATE_WALK1:
				if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count + time_action_ironman)) {
					lista_enemigos[id].state = STATE_WALK2;
					lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
					lista_enemigos[id].mov_count = glutGet(GLUT_ELAPSED_TIME);
				}
				break;
			case STATE_WALK2:
				if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count + time_action_ironman)) {
					lista_enemigos[id].state = STATE_WALK3;
					lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
					lista_enemigos[id].mov_count = glutGet(GLUT_ELAPSED_TIME);
				}
				break;
			case STATE_WALK3:
				if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count + time_action_ironman)) {
					lista_enemigos[id].state = STATE_WALK4;
					lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
					lista_enemigos[id].mov_count = glutGet(GLUT_ELAPSED_TIME);
				}
				break;
			case STATE_WALK4:
				if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count + time_action_ironman)) {
					lista_enemigos[id].state = STATE_WALK1;
					lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
					lista_enemigos[id].mov_count = glutGet(GLUT_ELAPSED_TIME);
				}
				break;
			case STATE_ATTACK1:
				if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count + time_action_ironman)) {
					lista_enemigos[id].state = STATE_ATTACK2;
					lista_enemigos[id].mov_count = glutGet(GLUT_ELAPSED_TIME);
				}
				break;
			case STATE_ATTACK2:
				if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count + time_action_ironman)) {
					lista_enemigos[id].state = STATE_ATTACK3;
					lista_enemigos[id].mov_count = glutGet(GLUT_ELAPSED_TIME);
				}
				break;
			case STATE_ATTACK3:
				if (glutGet(GLUT_ELAPSED_TIME) > (lista_enemigos[id].mov_count + time_action_ironman)) {
					if (lista_enemigos[id].right) {
						lista_enemigos[id].state = STATE_WALK1;
						AddGea(lista_enemigos[id].x + lista_enemigos[id].ancho, lista_enemigos[id].y + lista_enemigos[id].largo / 2, true);
						Sonido.Play(SOUND_IRON);
					}
					else {
						lista_enemigos[id].state = STATE_WALK1;
						AddGea(lista_enemigos[id].x, lista_enemigos[id].y + lista_enemigos[id].largo / 2, false);
						Sonido.Play(SOUND_IRON);
					}
					lista_enemigos[id].ancho = lista_enemigos[id].anchoo;
					lista_enemigos[id].x = lista_enemigos[id].xprev;
					lista_enemigos[id].mov_count = glutGet(GLUT_ELAPSED_TIME);
				}
				break;
		}
	}
}

void cEnemigo::Draw(int *map, int xmia, int ymia)
{

	float xo, yo, xf, yf;
	int i;
	for (i = 0; i < size_enemigos; i++) {
		if (lista_enemigos[i].active) {
			if (lista_enemigos[i].type != Enemy_zubat) {
				if (!CollidesMapFloorEn(map, i)) lista_enemigos[i].y -= (STEP_LENGTH);
			}
			switch (lista_enemigos[i].type)
			{
			case Enemy_Ironman:
				IA_ironman(i, map, xmia, ymia);
				break;
			case Enemy_Gohan:
				IA_Gohan(i, map, xmia, ymia);
				break;
			case Enemy_zubat:
				IA_Zubat(i);
				break;
			}

			switch (lista_enemigos[i].type)
			{
			case Enemy_Ironman:
				switch (lista_enemigos[i].state)
				{
				case STATE_WALK1:	
					if (lista_enemigos[i].right) {
						xo = 0.0f; yo = 0.25f; xf = xo + 0.1875f;
					}
					else {
						xo = 0.1875f; yo = 0.25f; xf = xo + 0.1875f;
					}
					break;
				case STATE_WALK2:	
					if (lista_enemigos[i].right) {
						xo = 0.0f; yo = 0.5f; xf = xo + 0.1875f;
					}
					else {
						xo = 0.1875f; yo = 0.5f; xf = xo + 0.1875f;
					}
					break;
				case STATE_WALK3:
					if (lista_enemigos[i].right) {
						xo = 0.0f; yo = 0.75f; xf = xo + 0.1875f;
					}
					else {
						xo = 0.1875f; yo = 0.75f; xf = xo + 0.1875f;
					}
					break;
				case STATE_WALK4:
					if (lista_enemigos[i].right) {
						xo = 0.0f; yo = 1.0f; xf = xo + 0.1875f;
					}
					else {
						xo = 0.1875f; yo = 1.0f; xf = xo + 0.1875f;
					}
					break;
				case STATE_ATTACK1:
					if (lista_enemigos[i].right) xo = 0.375f;
					else xo = 0.6875f;
					xf = xo + 0.3125;
					yo = 0.25f;
					break;
				case STATE_ATTACK2:
					if (lista_enemigos[i].right) xo = 0.375f;
					else xo = 0.6875f;
					xf = xo + 0.3125;
					yo = 0.5f;
					break;
				case STATE_ATTACK3:
					if (lista_enemigos[i].right) xo = 0.375f;
					else xo = 0.6875f;
					xf = xo + 0.3125;
					yo = 0.75f;
					break;
				}
				yf = yo - 0.25f;
				DrawRectEn(4, xo, yo, xf, yf, lista_enemigos[i].x, lista_enemigos[i].y, lista_enemigos[i].ancho, lista_enemigos[i].largo);
				break;

			case Enemy_Gohan:
				switch (lista_enemigos[i].state)
				{
				case STATE_WALK1:
					if (lista_enemigos[i].right) {
						xo = 0.0f; xf = xo + 0.125f;
						yo = 0.25f; 
					}
					else {
						xo = 0.125f;  xf = xo - 0.125f;
						yo = 0.25f;
					}
					yf = yo - 0.25f;
					break;
				case STATE_WALK2:
					if (lista_enemigos[i].right) {
						xo = 0.0f; xf = xo + 0.125f;
						yo = 0.5f;
					}
					else {
						xo = 0.125f;  xf = xo - 0.125f;
						yo = 0.5f;
					}
					yf = yo - 0.25f;
					break;
				case STATE_WALK3:
					if (lista_enemigos[i].right) {
						xo = 0.0f; xf = xo + 0.125f;
						yo = 0.75f;
					}
					else {
						xo = 0.125f;  xf = xo - 0.125f;
						yo = 0.75f;
					}
					yf = yo - 0.25f;
					break;
				case STATE_WALK4:
					if (lista_enemigos[i].right) {
						xo = 0.0f; xf = xo + 0.0625f;
						yo = 1.0f;
					}
					else {
						xo = 0.0625f;  xf = xo - 0.0625f;
						yo = 1.0f;
					}
					yf = yo - 0.1875f;
					break;
				case STATE_ATTACK1:
					if (lista_enemigos[i].right) {
						xo = 0.375f; xf = xo + 0.0625f;
						yo = 0.25f;
					}
					else {
						xo = 0.4375f;  xf = xo - 0.0625f;
						yo = 0.25f;
					}
					yf = yo - 0.1875f;
					break;
				case STATE_ATTACK2:
					if (lista_enemigos[i].right) {
						xo = 0.375f; xf = xo + 0.0625f;
						yo = 0.5f;
					}
					else {
						xo = 0.4375f;  xf = xo - 0.0625f;
						yo = 0.5f;
					}
					yf = yo - 0.1875f;
					break;
				case STATE_ATTACK3:
					if (lista_enemigos[i].right) {
						xo = 0.375f; xf = xo + 0.375f;
						yo = 0.75f;
					}
					else {
						xo = 0.75f;  xf = xo - 0.375f;
						yo = 0.75f;
					}
					yf = yo - 0.1875f;
					break;
				case STATE_ATTACK4:
					if (lista_enemigos[i].right) {
						xo = 0.375f; xf = xo + 0.375f;
						yo = 1.0f;
					}
					else {
						xo = 0.75f;  xf = xo - 0.375f;
						yo = 1.0f;
					}
					yf = yo - 0.1875f;
					break;
				}
				DrawRectEn(5, xo, yo, xf, yf, lista_enemigos[i].x, lista_enemigos[i].y, lista_enemigos[i].ancho, lista_enemigos[i].largo);
				break;

			case Enemy_zubat:
				switch (lista_enemigos[i].state)
				{
				case STATE_WALK1:
					if (lista_enemigos[i].right) {
						xo = 1.0f; yo = 0.25f; xf = xo - 1.0f;
					}
					else {
						xo = 0.0f; yo = 0.25f; xf = xo + 1.0f;
					}
					break;
				case STATE_WALK2:
					if (lista_enemigos[i].right) {
						xo = 1.0f; yo = 0.5f; xf = xo - 1.0f;
					}
					else {
						xo = 0.0f; yo = 0.5f; xf = xo + 1.0f;
					}
					break;
				case STATE_WALK3:
					if (lista_enemigos[i].right) {
						xo = 1.0f; yo = 0.75f; xf = xo - 1.0f;
					}
					else {
						xo = 0.0f; yo = 0.75f; xf = xo + 1.0f;
					}
					break;
				case STATE_WALK4:
					if (lista_enemigos[i].right) {
						xo = 1.0f; yo = 1.0f; xf = xo - 1.0f;
					}
					else {
						xo = 0.0f; yo = 1.0f; xf = xo + 1.0f;
					}
					break;
				}
				yf = yo - 0.25f;
				DrawRectEn(14, xo, yo, xf, yf, lista_enemigos[i].x, lista_enemigos[i].y, lista_enemigos[i].ancho, lista_enemigos[i].largo);
				break;
			}
		}
	}
	for (i = 0; i < size_gea; i++) {
		xo = 0.5f;
		yo = 0.5f + 0.0625f - (0.25f*(lista_geas[i].count % 3));
		xf = xo + 0.25f;
		yf = yo - 0.0625f;
		if (lista_geas[i].active) {
			DrawRectGea(3, xo, yo, xf, yf, lista_geas[i].x, lista_geas[i].y);
			lista_geas[i].count -= 1;
			if (lista_geas[i].count == 0) lista_geas[i].active = false;
			if (lista_geas[i].right) lista_geas[i].x += speed_gea;
			else lista_geas[i].x -= speed_gea;
		}
	}
}

void cEnemigo::DrawRectEn(int tex_id, float xo, float yo, float xf, float yf, float scx, float scy, int ancho, int largo)
{
	int screen_x, screen_y;
	screen_x = scx + SCENE_Xo;
	screen_y = scy + SCENE_Yo + (BLOCK_SIZE - TILE_SIZE);

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, tex_id);
	glBegin(GL_QUADS);
	glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
	glTexCoord2f(xf, yo);	glVertex2i(screen_x + ancho, screen_y);
	glTexCoord2f(xf, yf);	glVertex2i(screen_x + ancho, screen_y + largo);
	glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + largo);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

bool cEnemigo::CollidesMapFloorEn(int *map, int id)
{
	int tile_x, tile_y;
	int width_tiles;
	bool on_base;
	int i;

	tile_x = (lista_enemigos[id].x) / TILE_SIZE;
	tile_y = (lista_enemigos[id].y) / TILE_SIZE;

	width_tiles = lista_enemigos[id].ancho / TILE_SIZE;
	if ((lista_enemigos[id].x % TILE_SIZE) != 0) width_tiles++;

	on_base = false;
	i = 0;
	while ((i < width_tiles) && !on_base)
	{
		if ((lista_enemigos[id].y % TILE_SIZE) == 0)
		{
			if (map[(tile_x + i) + ((tile_y - 1) * SCENE_WIDTH)] == 1)
				on_base = true;
			/*else if (map[(tile_x + i) + ((tile_y - 1) * SCENE_WIDTH)] == 19)
			on_base = true;
			else if (map[(tile_x + i) + ((tile_y - 1) * SCENE_WIDTH)] == 20)
			on_base = true;*/
		}
		else
		{
			if (map[(tile_x + i) + (tile_y * SCENE_WIDTH)] == 1)
			{
				lista_enemigos[id].y = (tile_y + 1) * TILE_SIZE;
				on_base = true;
			}
			/*else if (map[(tile_x + i) + (tile_y * SCENE_WIDTH)] == 19)
			{
			y = (tile_y + 1) * TILE_SIZE;
			on_base = true;
			}
			else if (map[(tile_x + i) + (tile_y * SCENE_WIDTH)] == 20)
			{
			y = (tile_y + 1) * TILE_SIZE;
			on_base = true;
			}*/
		}
		i++;
	}
	return on_base;
}

void cEnemigo::SetWidthHeight(int w, int h, int id) 
{
	lista_enemigos[id].ancho = w;
	lista_enemigos[id].largo = h;
}

void cEnemigo::AddGea(int px, int py, bool pright) {
	if (index_gea >= size_gea) index_gea = 0;
	lista_geas[index_gea].x = px;
	lista_geas[index_gea].y = py;
	lista_geas[index_gea].right = pright;
	lista_geas[index_gea].active = true;
	lista_geas[index_gea].count = count_gea;
	index_gea += 1;
}

void cEnemigo::DrawRectGea(int tex_id, float xo, float yo, float xf, float yf, float scx, float scy)
{
	int screen_x, screen_y;
	screen_x = scx + SCENE_Xo;
	screen_y = scy + SCENE_Yo + (BLOCK_SIZE - TILE_SIZE);

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, tex_id);
	glBegin(GL_QUADS);
	glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
	glTexCoord2f(xf, yo);	glVertex2i(screen_x + width_gea, screen_y);
	glTexCoord2f(xf, yf);	glVertex2i(screen_x + width_gea, screen_y + height_gea);
	glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + height_gea);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

int cEnemigo::CollidesHero(int x, int y, int w, int h)
{
	int i;
	bool c = false;
	bool c2 = false;
	for (i = 0; i < size_enemigos; i++) {
		c = false;
		c2 = false;
		if (lista_enemigos[i].active) {
			if (w >= lista_enemigos[i].ancho) { //H�roe m�s ancho
				if (((lista_enemigos[i].x + lista_enemigos[i].ancho) >= x) & ((lista_enemigos[i].x + lista_enemigos[i].ancho) <= (x + w))) c = true;
				else if ((lista_enemigos[i].x >= x) & ((lista_enemigos[i].x) <= (x + w))) c = true;
			}
			else { //H�roe m�s estrecho
				if (((x + w) > lista_enemigos[i].x) & ((x + w) < (lista_enemigos[i].x + lista_enemigos[i].ancho))) c = true;
				else if ((x > lista_enemigos[i].x) & (x < (lista_enemigos[i].x + lista_enemigos[i].ancho))) c = true;
			}

			if (c) { //si colisiona con X
				if (h >= lista_enemigos[i].largo) { //H�roe m�s largo
					if (((lista_enemigos[i].y + lista_enemigos[i].largo) >= y) & ((lista_enemigos[i].y + lista_enemigos[i].largo) <= (y + h))) c2 = true;
					else if ((lista_enemigos[i].y >= y) & ((lista_enemigos[i].y) <= (y + h))) c2 = true;
				}
				else { //H�roe m�s corto
					if (((y + h) > lista_enemigos[i].y) & ((y + h) < (lista_enemigos[i].y + lista_enemigos[i].largo))) c2 = true;
					else if ((y > lista_enemigos[i].y) & (y < (lista_enemigos[i].y + lista_enemigos[i].largo))) c2 = true;
				}
			}
		}
		if (c2) {
			Sonido.Play(SOUND_DMG);
			switch (lista_enemigos[i].type)
			{
			case Enemy_Gohan:
				return 20;
				break;
			case Enemy_Ironman:
				return 15;
				break;
			case Enemy_zubat:
				return 10;
				break;
			}
		}
	}
	return CollidesGea(x, y, w, h);
}

int cEnemigo::CollidesGea(int x, int y, int w, int h)
{
	int i;
	bool c = false;
	bool c2 = false;
	for (i = 0; i < size_gea; i++) {
		c = false;
		c2 = false;
		int gx = lista_geas[i].x;
		int gy = lista_geas[i].y;
		if (lista_geas[i].active){
			if (w >= width_gea) { //H�roe m�s ancho
				if (((gx + width_gea) >= x) & ((gx + width_gea) <= (x + w))) c = true;
				else if ((gx >= x) & ((gx) <= (x + w))) c = true;
			}
			else { //H�roe m�s estrecho
				if (((x + w) > gx) & ((x + w) < (gx + width_gea))) c = true;
				else if ((x > gx) & (x < (gx + width_gea))) c = true;
			}

			if (c) { //si colisiona con X
				if (h >= height_gea) { //H�roe m�s largo
					if (((gy + height_gea) >= y) & ((gy + height_gea) <= (y + h))) c2 = true;
					else if ((lista_geas[i].y >= y) & ((gy) <= (y + h))) c2 = true;
				}
				else { //H�roe m�s corto
					if (((y + h) > gy) & ((y + h) < (gy + height_gea))) c2 = true;
					else if ((y > gy) & (y < (gy + height_gea))) c2 = true;
				}
			}
			if (c2){
				Sonido.Play(SOUND_DMG);
				return 20;
			}
		}
	}
	return 0;
}

void cEnemigo::CollidesEnergy(Objeto* list, int d)
{
	int i, j, gx, gy, x, y, w, h;
	bool c = false;
	bool c2 = false;
	for (i = 0; i < size_cruz; i++) {
		if (list[i].active) {
			gx = list[i].x;
			gy = list[i].y;
			for (j = 0; j < size_enemigos; j++) {
				c = false;
				c2 = false;
				x = lista_enemigos[j].x;
				y = lista_enemigos[j].y;
				w = lista_enemigos[j].ancho;
				h = lista_enemigos[j].largo;
				if (lista_enemigos[j].active){
					if (w >= list[i].ancho) { //Enemigo m�s ancho
						if (((gx + list[i].ancho) >= x) & ((gx + list[i].ancho) <= (x + w))) c = true;
						else if ((gx >= x) & ((gx) <= (x + w))) c = true;
					}
					else { //Enemigo m�s estrecho
						if (((x + w) > gx) & ((x + w) < (gx + list[i].ancho))) c = true;
						else if ((x > gx) & (x < (gx + list[i].ancho))) c = true;
					}

					if (c) { //si colisiona con X
						if (h >= list[i].largo) { //Enemigo m�s largo
							if (((gy + list[i].largo) >= y) & ((gy + list[i].largo) <= (y + h))) c2 = true;
							else if ((gy >= y) & ((gy) <= (y + h))) c2 = true;
						}
						else { //Enemigo m�s corto
							if (((y + h) > gy) & ((y + h) < (gy + list[i].largo))) c2 = true;
							else if ((y > gy) & (y < (gy + list[i].largo))) c2 = true;
						}
					}
				}
				if (c2) {
					lista_enemigos[j].hp -= d;
					if (lista_enemigos[j].type == Enemy_Ironman) {
						if (lista_enemigos[j].right & (list[i].x < lista_enemigos[j].x)) lista_enemigos[j].right = false;
						else if (!lista_enemigos[j].right & (list[i].x > lista_enemigos[j].x)) lista_enemigos[j].right = true;
					}
					if (lista_enemigos[j].hp <= 0) {
						Sonido.Play(SOUND_KILL);
						lista_enemigos[j].active = false;
						lista_enemigos[j].count_respawn = glutGet(GLUT_ELAPSED_TIME);
					}
					list[i].active = false;
					if (list[i].largo > 32 & list[i].ancho > 32) list[i].active = true;
					j = size_enemigos;
				}
			}
		}
	}
	
}

void cEnemigo::Reset()
{
	int i;
	for (i = 0; i < size_enemigos; i++){
		lista_enemigos[i].x = 0; lista_enemigos[i].xo = 0;
		lista_enemigos[i].y = 0; lista_enemigos[i].yo = 0;
		lista_enemigos[i].right = false;
		lista_enemigos[i].active = false;
		lista_enemigos[i].respawn_time = 0;
		lista_enemigos[i].largo = 0;
		lista_enemigos[i].ancho = 0;
		lista_enemigos[i].largoo = 0;
		lista_enemigos[i].anchoo = 0;
		lista_enemigos[i].type = 0;
		lista_enemigos[i].mov_count = 0;
		lista_enemigos[i].hp = 0;
		lista_enemigos[i].last_action = 0;
		lista_enemigos[i].state = 0;
		lista_enemigos[i].xprev = 0;
		lista_enemigos[i].yprev = 0;
	}
	for (i = 0; i < size_gea; i++) {
		lista_geas[i].active = false;
		lista_geas[i].count = 0;
		lista_geas[i].right = false;
		lista_geas[i].x = 0;
		lista_geas[i].y = 0;
	}
}

void cEnemigo::RespawnEnemies(int camx, int camy)
{
	for (index_enemigo = 0; index_enemigo < size_enemigos; index_enemigo++) {
		if (lista_enemigos[index_enemigo].active == false && lista_enemigos[index_enemigo].respawn_time > 0) {
			if (glutGet(GLUT_ELAPSED_TIME) >= (lista_enemigos[index_enemigo].count_respawn + lista_enemigos[index_enemigo].respawn_time)) {
				if (lista_enemigos[index_enemigo].type == Enemy_zubat) {
					lista_enemigos[index_enemigo].active = true;
					lista_enemigos[index_enemigo].x = lista_enemigos[index_enemigo].xo;
					lista_enemigos[index_enemigo].y = lista_enemigos[index_enemigo].yo;
					lista_enemigos[index_enemigo].mov_count = 0;
					lista_enemigos[index_enemigo].mov_count_att = 0;
					lista_enemigos[index_enemigo].mov_count = 240;
					lista_enemigos[index_enemigo].hp = hp_zubat;
					lista_enemigos[index_enemigo].last_action = glutGet(GLUT_ELAPSED_TIME);
					lista_enemigos[index_enemigo].state = STATE_WALK1;
				}
				if ((lista_enemigos[index_enemigo].xo < (camx - camx) | lista_enemigos[index_enemigo].xo > (camx + 1200)) | (lista_enemigos[index_enemigo].yo < (camy - camy) | lista_enemigos[index_enemigo].yo > (camy + 960))) {
					lista_enemigos[index_enemigo].active = true;
					lista_enemigos[index_enemigo].x = lista_enemigos[index_enemigo].xo;
					lista_enemigos[index_enemigo].y = lista_enemigos[index_enemigo].yo;
					lista_enemigos[index_enemigo].mov_count = 0;
					lista_enemigos[index_enemigo].mov_count_att = 0;
					if (lista_enemigos[index_enemigo].type == Enemy_zubat) {
						lista_enemigos[index_enemigo].mov_count = 240;
						lista_enemigos[index_enemigo].hp = hp_zubat;
					}
					else if (lista_enemigos[index_enemigo].type == Enemy_Ironman) {
						lista_enemigos[index_enemigo].hp = hp_ironman;
					}
					else if (lista_enemigos[index_enemigo].type == Enemy_Gohan) {
						lista_enemigos[index_enemigo].hp = hp_gohan;
					}
					lista_enemigos[index_enemigo].last_action = glutGet(GLUT_ELAPSED_TIME);
					lista_enemigos[index_enemigo].state = STATE_WALK1;
				}
			}
		}
	}
}

bool cEnemigo::LoadEnemigos(int level)
{
	bool res = true;;
	FILE *fd;
	char file[16];
	char t1;
	char t2;
	int cuantos, cx, cy, t, cr, cd;
	if (level < 10) sprintf(file, "%s0%d%s", (char *)"enemigos", level, (char *)FILENAME_EXT);
	else			sprintf(file, "%s%d%s", (char *)"enemigos", level, (char *)FILENAME_EXT);
	fd = fopen(file, "r");
	if (fd == NULL) return false;

	fscanf(fd, "%c", &t1); //read cuantos enemigos
	fscanf(fd, "%c", &t2);
	cuantos = ((t1 - 48) * 10) + (t2 - 48);
	fscanf(fd, "%c", &t1); //read intro
	while (cuantos > 0) {

		fscanf(fd, "%c", &t1); //coordenada X
		cx = (t1 - 48);
		fscanf(fd, "%c", &t2);
		if (t2 != ','){
			cx = (cx * 10);
			cx = cx + (t2 - 48);
			fscanf(fd, "%c", &t1);
		}

		fscanf(fd, "%c", &t1); //coordenada Y
		cy = (t1 - 48);
		fscanf(fd, "%c", &t2);
		if (t2 != ','){
			cy = (cy * 10);
			cy = cy + (t2 - 48);
			fscanf(fd, "%c", &t1);
		}

		fscanf(fd, "%c", &t1); //Type enemigo
		t = (t1 - 48);
		fscanf(fd, "%c", &t2);
		if (t2 != ','){
			t = (t * 10);
			t = t + (t2 - 48);
			fscanf(fd, "%c", &t1);
		}

		fscanf(fd, "%c", &t1); //respawn
		cr = (t1 - 48);
		fscanf(fd, "%c", &t2);
		if (t2 != ','){
			cr = (cr * 10);
			cr = cr + (t2 - 48);
			fscanf(fd, "%c", &t1);
		}

		fscanf(fd, "%c", &t1); //right
		cd = (t1 - 48);
		fscanf(fd, "%c", &t2);
		if (t2 != ','){
			cd = (cd * 10);
			cd = cd + (t2 - 48);
			fscanf(fd, "%c", &t1);
		}

		AddEnemigo((cx*TILE_SIZE), (cy*TILE_SIZE), t, cr, cd);
		cuantos -= 1;
		fscanf(fd, "%c", &t1); //leer intro
	}
	fclose(fd);
	return res;
}
