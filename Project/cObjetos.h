#pragma once
#include "cTexture.h"
#include "Globals.h"
#include "cSound.h"

#define size_cruz			25
#define speed_cruz			4
#define count_cruz			150	
#define size_objeto			99
#define t_llavecofre		1
#define t_llavepuerta		2
#define t_cofre				3
#define t_vida				4
#define t_hp				5
#define t_fuego				6
#define t_velita			7
#define t_paredfalsa		8
#define t_throw				9
#define t_reemplazo			10
#define t_supalatigo		11
#define t_puerta			12

struct Objeto
{
	bool active;
	int x;
	int y;
	bool right;
	unsigned int count;
	int ancho, largo;
	int type;
	int type_al_romper;
};

class cObjetos
{
public:
	cObjetos();
	~cObjetos();

	Objeto lista_objetos[size_objeto];
	Objeto lista_cruces[size_cruz];	

	cSound Sonido;
	int index_cruz;
	int index_objeto;

	void Draw(int text_id, int l);
	void DrawRectOb(int tex_id, float xo, float yo, float xf, float yf, float scx, float scy, int id);
	void DrawRectCr(int tex_id, float xo, float yo, float xf, float yf, float scx, float scy, int id);
	void DrawRectObjetoGuarro(int tex_id, float xo, float yo, float xf, float yf, float scx, float scy, int ancho, int largo);
	void AddCruz(int x, int y, bool right, bool turb);
	void AddObjeto(int px, int py, int ancho, int largo, int type, int tr);
	bool LoadItems(int level);
	void CollidesAttack(int x, int y, int w, int h, int *map);
	void CollidesThrows(int *map);
	int CollidesHeroe(int *map, int x, int y, int w, int h, int *hp, int *lives, int *throwa, int *llave, bool *supalatigo);
	void Reset();
	int TOb();
	struct Objeto *GetListaCruces();

	
};

