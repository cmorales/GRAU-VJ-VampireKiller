#include "cSound.h"

cSound::cSound(void)
{
	FMOD::System_Create(&system);// create an instance of the game engine
	system->init(32, FMOD_INIT_NORMAL, 0);// initialise the game engine with 32 channels (cantidad de sonido simultaneo que puede haber)
}

cSound::~cSound(void)
{
	for (int i = 0; i < NUM_SOUNDS; i++) sounds[i]->release();
	system->release();
}

bool cSound::Load()
{
	system->createStream("Sounds/song3.wav", FMOD_SOFTWARE | FMOD_LOOP_NORMAL, 0, &sounds[SOUND_MENU]);
	system->createStream("Sounds/song2.wav", FMOD_SOFTWARE | FMOD_LOOP_NORMAL, 0, &sounds[SOUND_AMBIENT1]);
	system->createStream("Sounds/song.wav", FMOD_SOFTWARE | FMOD_LOOP_NORMAL, 0, &sounds[SOUND_AMBIENT2]);
	system->createSound("Sounds/whip.wav", FMOD_HARDWARE, 0, &sounds[SOUND_LATIGO]);

	system->createSound("Sounds/heart.wav", FMOD_HARDWARE, 0, &sounds[SOUND_RVIDA]);
	system->createSound("Sounds/Energy Restore.wav", FMOD_HARDWARE, 0, &sounds[SOUND_RHP]);
	system->createSound("Sounds/recogerobj.wav", FMOD_HARDWARE, 0, &sounds[SOUND_RMUNI]);
	system->createSound("Sounds/Hard Landing.wav", FMOD_HARDWARE, 0, &sounds[SOUND_CAER]);
	system->createSound("Sounds/Menu Selection.wav", FMOD_HARDWARE, 0, &sounds[SOUND_SELECT]);
	system->createSound("Sounds/Menu Entry.wav", FMOD_HARDWARE, 0, &sounds[SOUND_ENTRY]);
	system->createSound("Sounds/Pause.wav", FMOD_HARDWARE, 0, &sounds[SOUND_PAUSE]);
	system->createSound("Sounds/death.wav", FMOD_HARDWARE, 0, &sounds[SOUND_MUERTE]);
	system->createSound("Sounds/destruction.wav", FMOD_HARDWARE, 0, &sounds[SOUND_THROW]);
	system->createSound("Sounds/DMG.wav", FMOD_HARDWARE, 0, &sounds[SOUND_DMG]);
	system->createSound("Sounds/power.wav", FMOD_HARDWARE, 0, &sounds[SOUND_POWER]);
	system->createSound("Sounds/iron.wav", FMOD_HARDWARE, 0, &sounds[SOUND_IRON]);
	system->createSound("Sounds/bola.wav", FMOD_HARDWARE, 0, &sounds[SOUND_BOLA]);
	system->createSound("Sounds/kame.wav", FMOD_HARDWARE, 0, &sounds[SOUND_KAME]);
	system->createSound("Sounds/Kill.wav", FMOD_HARDWARE, 0, &sounds[SOUND_KILL]);
	system->createSound("Sounds/fail.wav", FMOD_HARDWARE, 0, &sounds[SOUND_FAIL]);
	system->createSound("Sounds/petar.wav", FMOD_HARDWARE, 0, &sounds[SOUND_PETAR]);
	system->createSound("Sounds/Stage Clear.wav", FMOD_HARDWARE, 0, &sounds[SOUND_STAGE]);
	return true;
}

void cSound::Play(int sound_id)
{
	if (sound_id == SOUND_AMBIENT1) {
		system->playSound(FMOD_CHANNEL_FREE, sounds[SOUND_AMBIENT1], false, &ambient1Channel);
		ambient1Channel->setVolume(1.0f); //between 0 and 1
	}
	else if (sound_id == SOUND_AMBIENT2) {
		system->playSound(FMOD_CHANNEL_FREE, sounds[SOUND_AMBIENT2], false, &ambient2Channel);
		ambient2Channel->setVolume(1.0f); //between 0 and 1
	}
	else if (sound_id == SOUND_MENU) {
		system->playSound(FMOD_CHANNEL_FREE, sounds[SOUND_MENU], false, &menuChannel);
		menuChannel->setVolume(1.0f); //between 0 and 1
	}
	else system->playSound(FMOD_CHANNEL_FREE, sounds[sound_id], false, 0);
}

void cSound::StopAll()
{
	ambient1Channel->stop();
	ambient2Channel->stop();
	menuChannel->stop();
}

void cSound::Update()
{
	system->update();
}