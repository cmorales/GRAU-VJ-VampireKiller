#include "cObjetos.h"
#include "cTexture.h"
#include "Globals.h"
#include "cScene.h"
#include "cData.h"
#include "cBicho.h"
#include "cPlayer.h"

cObjetos::cObjetos()
{
	Sonido.Load();	
	index_cruz = 0;
}


cObjetos::~cObjetos()
{
}

struct Objeto* cObjetos::GetListaCruces()
{
	return lista_cruces;
}

void cObjetos::AddCruz(int px, int py, bool pright, bool turb) {
	if (index_cruz >= size_cruz) index_cruz = 0;
	lista_cruces[index_cruz].x = px;
	lista_cruces[index_cruz].y = py;
	lista_cruces[index_cruz].right = pright;
	lista_cruces[index_cruz].active = true;
	lista_cruces[index_cruz].count = count_cruz;
	if (turb) {
		lista_cruces[index_cruz].ancho = 96;
		lista_cruces[index_cruz].largo = 96;
	}
	else {
		lista_cruces[index_cruz].ancho = 32;
		lista_cruces[index_cruz].largo = 24;
	}
	index_cruz += 1;
}

void cObjetos::AddObjeto(int px, int py, int ancho, int largo, int type, int tr) {
	for (index_objeto = 0; index_objeto < size_objeto; index_objeto++) {
		if (lista_objetos[index_objeto].active == false) {
			lista_objetos[index_objeto].x = px;
			lista_objetos[index_objeto].y = py;
			lista_objetos[index_objeto].active = true;
			lista_objetos[index_objeto].count = 0;
			lista_objetos[index_objeto].ancho = ancho;
			lista_objetos[index_objeto].largo = largo;
			lista_objetos[index_objeto].type = type;
			lista_objetos[index_objeto].type_al_romper = tr;
			return;
		}
	}
}

int n1 = 10;
int n2 = 0;
int o1 = 10;
int o2 = 0;
void cObjetos::Draw(int text_id, int l) {
	float xo, yo, xf, yf;
	int i;
	n1 -= 1;
	if (n1 == 0){
		n1 = 10;
		n2 += 1;
		if (n2 >= 4) n2 = 0;
	}
	o1 -= 1;
	if (o1 == 0){
		o1 = 10;
		o2 += 1;
		if (o2 >= 2) o2 = 0;
	}
	for (i = 0; i < size_objeto; i++) {
		if (lista_objetos[i].active) {
			if (lista_objetos[i].type == t_reemplazo) {
				xo = 0.4f + (l-1)*0.1f;
				yo = 0.2f;
				xf = xo + 0.1f;
				yf = yo - 0.2f;
				DrawRectOb(10, xo, yo, xf, yf, lista_objetos[i].x, lista_objetos[i].y, i);
			}
		}
	}
	for (i = 0; i < size_objeto; i++) {
		if (lista_objetos[i].active && lista_objetos[i].type != t_reemplazo & lista_objetos[i].active && lista_objetos[i].type != t_paredfalsa) {
			switch (lista_objetos[i].type)
			{
			case t_llavecofre:
				xo = 0.0f; yo = 0.2f;
				break;
			case t_llavepuerta:
				xo = 0.0f; yo = 0.1f;
				break;
			case t_cofre:
				xo = 0.0f; yo = 0.3f;
				break;
			case t_vida:
				xo = 0.0f; yo = 0.4f;
				break;
			case t_hp:
				xo = 0.0f; yo = 0.5f;
				break;
			case t_fuego:
				xo = 0.1f; yo = 0.1f + (o2 * 0.1f);
				break;
			case t_velita:
				xo = 0.2f; yo = 0.1f + (o2 * 0.1f);
				break;
			case t_paredfalsa:
				xo = 0.0f; yo = 0.8f;
				break;
			case t_throw:
				xo = 0.0f; yo = 0.9f;
				break;
			case t_supalatigo:
				xo = 0.0f; yo = 0.6f;
				break;
			case t_puerta:
				xo = 0.6f + (l-1)*0.2f; yo = 0.3f;
				break;
			}
			xf = xo + 0.1f; yf = yo - 0.1f;
			if (lista_objetos[i].type == t_paredfalsa) yf = yo - 0.2f;
			if (lista_objetos[i].type == t_puerta) {
				yf = yo - 0.3f;
				xf = xo + 0.2f;
			}
			DrawRectOb(10, xo, yo, xf, yf, lista_objetos[i].x, lista_objetos[i].y, i);
		}
	}
	for (i = 0; i < size_objeto; i++) {
		if (lista_objetos[i].active) {
			if (lista_objetos[i].type == t_paredfalsa) {
				xo = 0.0f + (l-1)*0.1f; yo = 0.8f;
				xf = xo + 0.1f;
				yf = yo - 0.2f;
				DrawRectOb(10, xo, yo, xf, yf, lista_objetos[i].x, lista_objetos[i].y, i);
			}
		}
	}
	for (i = 0; i < size_cruz; i++) {
		if (lista_cruces[i].right) {
			xo = 0.0f;
			xf = xo + 0.25f;
		}
		else {
			xo = 0.25f;
			xf = xo - 0.25f;
		}
		yo = 0.9375f - (0.25f*(n2));
		//xf = xo + 0.25F;
		yf = yo - 0.1875f;
		if (lista_cruces[i].active) {
			DrawRectCr(text_id, xo, yo, xf, yf, lista_cruces[i].x, lista_cruces[i].y, i);
			lista_cruces[i].count -= 1;
			if (lista_cruces[i].count == 0) lista_cruces[i].active = false;
			if (lista_cruces[i].right) lista_cruces[i].x += speed_cruz;
			else lista_cruces[i].x -= speed_cruz;
		}
	}
}

void cObjetos::DrawRectOb(int tex_id, float xo, float yo, float xf, float yf, float scx, float scy, int id)
{
	int screen_x, screen_y;
	screen_x = scx + SCENE_Xo;
	screen_y = scy + SCENE_Yo + (BLOCK_SIZE - TILE_SIZE);

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, tex_id);
	glBegin(GL_QUADS);
	glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
	glTexCoord2f(xf, yo);	glVertex2i(screen_x + lista_objetos[id].ancho, screen_y);
	glTexCoord2f(xf, yf);	glVertex2i(screen_x + lista_objetos[id].ancho, screen_y + lista_objetos[id].largo);
	glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + lista_objetos[id].largo);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

void cObjetos::DrawRectCr(int tex_id, float xo, float yo, float xf, float yf, float scx, float scy, int id)
{
	int screen_x, screen_y;
	screen_x = scx + SCENE_Xo;
	screen_y = scy + SCENE_Yo + (BLOCK_SIZE - TILE_SIZE);

	if (lista_cruces[id].ancho > 32 & lista_cruces[id].largo > 32) {
		xo = 0.75; xf = 1.0f;
		yo = yo + 0.0625;  yf = yo - 0.25f;
	}

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, tex_id);
	glBegin(GL_QUADS);
		glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
		glTexCoord2f(xf, yo);	glVertex2i(screen_x + lista_cruces[id].ancho, screen_y);
		glTexCoord2f(xf, yf);	glVertex2i(screen_x + lista_cruces[id].ancho, screen_y + lista_cruces[id].largo);
		glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + lista_cruces[id].largo);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

void cObjetos::DrawRectObjetoGuarro(int tex_id, float xo, float yo, float xf, float yf, float scx, float scy, int ancho, int largo){
	int screen_x, screen_y;
	screen_x = scx + SCENE_Xo;
	screen_y = scy + SCENE_Yo + (BLOCK_SIZE - TILE_SIZE);

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, tex_id);
	glBegin(GL_QUADS);
	glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
	glTexCoord2f(xf, yo);	glVertex2i(screen_x + ancho, screen_y);
	glTexCoord2f(xf, yf);	glVertex2i(screen_x + ancho, screen_y + largo);
	glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + largo);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

bool cObjetos::LoadItems(int level)
{
	bool res = true;;
	FILE *fd;
	char file[16];	
	char t1;
	char t2;
	int cuantos, cx, cy, t, cw, ch, cr;
	if (level < 10) sprintf(file, "%s0%d%s", (char *)"objetos", level, (char *)FILENAME_EXT);
	else			sprintf(file, "%s%d%s", (char *)"objetos", level, (char *)FILENAME_EXT);
	fd = fopen(file, "r");
	if (fd == NULL) return false;

	fscanf(fd, "%c", &t1); //read cuantos items
	fscanf(fd, "%c", &t2);
	cuantos = ((t1 - 48) * 10) + (t2 - 48);
	fscanf(fd, "%c", &t1); //read intro
	while(cuantos > 0) {

		fscanf(fd, "%c", &t1); //coordenada X
		cx = (t1 - 48);
		fscanf(fd, "%c", &t2);
		if (t2 != ','){
			cx = (cx * 10);
			cx = cx + (t2 - 48);
			fscanf(fd, "%c", &t1); 
		}

		fscanf(fd, "%c", &t1); //coordenada Y
		cy = (t1 - 48);
		fscanf(fd, "%c", &t2);
		if (t2 != ','){
			cy = (cy * 10);
			cy = cy + (t2 - 48);
			fscanf(fd, "%c", &t1);
		}

		fscanf(fd, "%c", &t1); //Type objeto
		t = (t1 - 48);
		fscanf(fd, "%c", &t2);
		if (t2 != ','){
			t = (t * 10);
			t = t + (t2 - 48);
			fscanf(fd, "%c", &t1);
		}

		fscanf(fd, "%c", &t1); //anchura
		cw = (t1 - 48);
		fscanf(fd, "%c", &t2);
		if (t2 != ','){
			cw = (cw * 10);
			cw = cw + (t2 - 48);
			fscanf(fd, "%c", &t1);
		}

		fscanf(fd, "%c", &t1); //largura
		ch = (t1 - 48);
		fscanf(fd, "%c", &t2);
		if (t2 != ','){
			ch = (ch * 10);
			ch = ch + (t2 - 48);
			fscanf(fd, "%c", &t1);
		}

		fscanf(fd, "%c", &t1); //type_al_morir
		cr = (t1 - 48);
		fscanf(fd, "%c", &t2);
		if (t2 != ','){
			cr = (cr * 10);
			cr = cr + (t2 - 48);
			fscanf(fd, "%c", &t1);
		}

		AddObjeto((cx*TILE_SIZE), (cy*TILE_SIZE), cw, ch, t, cr);
		cuantos -= 1;
		fscanf(fd, "%c", &t1); //leer intro
	}
	fclose(fd);
	return res;
}

bool breakable(int t)
{
	if (t == t_fuego) return true;
	if (t == t_velita) return true;
	if (t == t_paredfalsa) return true;
	return false;
}

void cObjetos::CollidesAttack(int x, int y, int w, int h, int *map)
{
	int i;
	bool c;
	bool c2;
	for (i = 0; i < size_objeto; i++) {
		if (lista_objetos[i].active && breakable(lista_objetos[i].type)) {
			c = false;
			c2 = false;
			if (w >= lista_objetos[i].ancho) { //L�tigo m�s ancho
				if (((lista_objetos[i].x + lista_objetos[i].ancho) >= x) & ((lista_objetos[i].x + lista_objetos[i].ancho) <= (x + w))) c = true;
				else if ((lista_objetos[i].x >= x) & ((lista_objetos[i].x) <= (x + w))) c = true;
			}
			else { //L�tigo m�s estrecho
				if (((x + w) > lista_objetos[i].x) & ((x + w) < (lista_objetos[i].x + lista_objetos[i].ancho))) c = true;
				else if ((x > lista_objetos[i].x) & (x < (lista_objetos[i].x + lista_objetos[i].ancho))) c = true;
			}

			if (c) { //si colisiona con X
				if (h >= lista_objetos[i].largo) { //L�tigo m�s largo
					if (((lista_objetos[i].y + lista_objetos[i].largo) >= y) & ((lista_objetos[i].y + lista_objetos[i].largo) <= (y + h))) c2 = true;
					else if ((lista_objetos[i].y >= y) & ((lista_objetos[i].y) <= (y + h))) c2 = true;
				}
				else { //L�tigo m�s corto
					if (((y + h) > lista_objetos[i].y) & ((y + h) < (lista_objetos[i].y + lista_objetos[i].largo))) c2 = true;
					else if ((y > lista_objetos[i].y) & (y < (lista_objetos[i].y + lista_objetos[i].largo))) c2 = true;
				}
			}
			if (c2) {
				//lista_objetos[i].active = false;
				if (lista_objetos[i].type_al_romper != 0){
					int ty, tx, in;
					if (lista_objetos[i].type == t_paredfalsa){
						ty = (lista_objetos[i].y / TILE_SIZE);
						tx = (lista_objetos[i].x / TILE_SIZE);
						for (in = 0; in < (lista_objetos[i].largo / TILE_SIZE); in++, ty++) map[tx + (ty*SCENE_WIDTH)] = 3;
						lista_objetos[i].type = t_reemplazo;
						AddObjeto(lista_objetos[i].x, lista_objetos[i].y, 32, 32, lista_objetos[i].type_al_romper, 0);
					}
					else {
						lista_objetos[i].type = lista_objetos[i].type_al_romper;
						lista_objetos[i].largo = TILE_SIZE;
						lista_objetos[i].ancho = TILE_SIZE;
						ty = (lista_objetos[i].y / TILE_SIZE) - 1;
						tx = (lista_objetos[i].x / TILE_SIZE);
						while (map[(tx)+(ty * SCENE_WIDTH)] != 1) {
							ty = ty - 1;
							lista_objetos[i].y -= TILE_SIZE;
						}
					}
				}	
				else if (lista_objetos[i].type == t_paredfalsa) {
					int ty, tx, in;
					ty = (lista_objetos[i].y / TILE_SIZE);
					tx = (lista_objetos[i].x / TILE_SIZE);
					for (in = 0; in < (lista_objetos[i].largo / TILE_SIZE); in++, ty++) map[tx + (ty*SCENE_WIDTH)] = 3;
					lista_objetos[i].type = t_reemplazo;
				}
				else if (lista_objetos[i].type == t_reemplazo);
				else lista_objetos[i].active = false;
			}
		}
	}
}

void cObjetos::CollidesThrows(int *map) {
	int i, j;
	bool c;
	bool c2;
	for (i = 0; i < size_objeto; i++) {
		if (lista_objetos[i].active && breakable(lista_objetos[i].type)) {
			for (j = 0; j < size_cruz; j++) {
				if (lista_cruces[j].active) {
					int w2 = lista_cruces[j].ancho; int h2 = lista_cruces[j].largo;
					int x2 = lista_cruces[j].x; int y2 = lista_cruces[j].y;
					c = false;
					c2 = false;
					if (w2 >= lista_objetos[i].ancho) { //Cruz m�s ancho
						if (((lista_objetos[i].x + lista_objetos[i].ancho) >= x2) & ((lista_objetos[i].x + lista_objetos[i].ancho) <= (x2 + w2))) c = true;
						else if ((lista_objetos[i].x >= x2) & ((lista_objetos[i].x) <= (x2 + w2))) c = true;
					}
					else { //Cruz m�s estrecho
						if (((x2 + w2) > lista_objetos[i].x) & ((x2 + w2) < (lista_objetos[i].x + lista_objetos[i].ancho))) c = true;
						else if ((x2 > lista_objetos[i].x) & (x2 < (lista_objetos[i].x + lista_objetos[i].ancho))) c = true;
					}

					if (c) { //si colisiona con X
						if (h2 >= lista_objetos[i].largo) { //Cruz m�s largo
							if (((lista_objetos[i].y + lista_objetos[i].largo) >= y2) & ((lista_objetos[i].y + lista_objetos[i].largo) <= (y2 + h2))) c2 = true;
							else if ((lista_objetos[i].y >= y2) & ((lista_objetos[i].y) <= (y2 + h2))) c2 = true;
						}
						else { //Cruz m�s corto
							if (((y2 + h2) > lista_objetos[i].y) & ((y2 + h2) < (lista_objetos[i].y + lista_objetos[i].largo))) c2 = true;
							else if ((y2 > lista_objetos[i].y) & (y2 < (lista_objetos[i].y + lista_objetos[i].largo))) c2 = true;
						}
					}
					if (c2) {
						//lista_objetos[i].active = false;
						Sonido.Play(SOUND_PETAR);
						lista_cruces[j].active = false;
						if (lista_cruces[j].ancho > 32 & lista_cruces[j].largo > 32) lista_cruces[j].active = true;
						if (lista_objetos[i].type_al_romper != 0) {
							int ty, tx, in;
							if (lista_objetos[i].type == t_paredfalsa){
								ty = (lista_objetos[i].y / TILE_SIZE);
								tx = (lista_objetos[i].x / TILE_SIZE);
								for (in = 0; in < (lista_objetos[i].largo / TILE_SIZE); in++, ty++) map[tx + (ty*SCENE_WIDTH)] = 3;
								lista_objetos[i].type = t_reemplazo;
								AddObjeto(lista_objetos[i].x, lista_objetos[i].y, 32, 32, lista_objetos[i].type_al_romper, 0);
							}
							else {
								lista_objetos[i].type = lista_objetos[i].type_al_romper;
								lista_objetos[i].largo = TILE_SIZE;
								lista_objetos[i].ancho = TILE_SIZE;
								j = size_cruz;
								ty = (lista_objetos[i].y / TILE_SIZE) - 1;
								tx = (lista_objetos[i].x / TILE_SIZE);
								while (map[(tx)+(ty * SCENE_WIDTH)] != 1) {
									ty = ty - 1;
									lista_objetos[i].y -= TILE_SIZE;
								}
							}
						}
						else if (lista_objetos[i].type == t_paredfalsa){
							int ty, tx, in;
							ty = (lista_objetos[i].y / TILE_SIZE);
							tx = (lista_objetos[i].x / TILE_SIZE);
							for (in = 0; in < (lista_objetos[i].largo / TILE_SIZE); in++, ty++) map[tx + (ty*SCENE_WIDTH)] = 3;
							lista_objetos[i].type = t_reemplazo;
						}
						else lista_objetos[i].active = false;
					}
				}
			}
		}
	}
}

int cObjetos::CollidesHeroe(int *map, int x, int y, int w, int h, int *hp, int *lives, int *throwa, int *llave, bool *supalatigo)
{
	int i, j;
	bool c;
	bool c2;
	*hp = 0; *lives = 0; *throwa = 0;
	for (i = 0; i < size_objeto; i++) {
		if (lista_objetos[i].active && !breakable(lista_objetos[i].type)) {
			c = false;
			c2 = false;
			if (w >= lista_objetos[i].ancho) { //L�tigo m�s ancho
				if (((lista_objetos[i].x + lista_objetos[i].ancho) >= x) & ((lista_objetos[i].x + lista_objetos[i].ancho) <= (x + w))) c = true;
				else if ((lista_objetos[i].x >= x) & ((lista_objetos[i].x) <= (x + w))) c = true;
			}
			else { //L�tigo m�s estrecho
				if (((x + w) > lista_objetos[i].x) & ((x + w) < (lista_objetos[i].x + lista_objetos[i].ancho))) c = true;
				else if ((x > lista_objetos[i].x) & (x < (lista_objetos[i].x + lista_objetos[i].ancho))) c = true;
			}

			if (c) { //si colisiona con X
				if (h >= lista_objetos[i].largo) { //L�tigo m�s largo
					if (((lista_objetos[i].y + lista_objetos[i].largo) >= y) & ((lista_objetos[i].y + lista_objetos[i].largo) <= (y + h))) c2 = true;
					else if ((lista_objetos[i].y >= y) & ((lista_objetos[i].y) <= (y + h))) c2 = true;
				}
				else { //L�tigo m�s corto
					if (((y + h) > lista_objetos[i].y) & ((y + h) < (lista_objetos[i].y + lista_objetos[i].largo))) c2 = true;
					else if ((y > lista_objetos[i].y) & (y < (lista_objetos[i].y + lista_objetos[i].largo))) c2 = true;
				}
			}
			if (c2) {
				lista_objetos[i].active = false;
				if (lista_objetos[i].type == t_hp) {
					*hp = 10;
					Sonido.Play(SOUND_RHP);
				}
					
				if (lista_objetos[i].type == t_vida) {
					Sonido.Play(SOUND_RVIDA);
					*lives = 1;
					
				}

				if
					(lista_objetos[i].type == t_throw) {
						Sonido.Play(SOUND_RMUNI);
						*throwa = 5;
				}

				if 
					(lista_objetos[i].type == t_llavepuerta) { //0sin, 1cofre, 2puerta, 3ambas
						if (*llave == 0) {
							*llave = 2;
							Sonido.Play(SOUND_RMUNI);
						}
						else if (*llave == 1){
							*llave = 3;
							Sonido.Play(SOUND_RMUNI);
						}
					else {
						lista_objetos[i].active = true;
					}
				}
				if (lista_objetos[i].type == t_llavecofre) {
					if (*llave == 0){
						Sonido.Play(SOUND_RMUNI);
						*llave = 1;
					}
					else if (*llave == 2) {
						*llave = 3;
						Sonido.Play(SOUND_RMUNI);
					}
					else {
						lista_objetos[i].active = true;
					}
				}
				if (lista_objetos[i].type == t_cofre) {
					if (*llave == 1) {
						*llave = 0;
						Sonido.Play(SOUND_RMUNI);
						lista_objetos[i].type = lista_objetos[i].type_al_romper;
					}
					else if (*llave == 3) {
						*llave = 2;
						Sonido.Play(SOUND_RMUNI);
						lista_objetos[i].type = lista_objetos[i].type_al_romper;
					}
					lista_objetos[i].active = true;
					return 0;
				}
				if (lista_objetos[i].type == t_supalatigo) {
					//lista_objetos[i].active = true;
					Sonido.Play(SOUND_POWER);
					*supalatigo = true;
				}
				if (lista_objetos[i].type == t_puerta) {
					int tx = lista_objetos[i].x / TILE_SIZE;
					int ty;
					int in;
					if (*llave == 2) {
						*llave = 0;
						Sonido.Play(SOUND_STAGE);
						lista_objetos[i].type = t_reemplazo;
						for (in = 0, ty = lista_objetos[i].y / TILE_SIZE; in < (lista_objetos[i].largo / TILE_SIZE); in++, ty++) map[tx + (ty*SCENE_WIDTH)] = 3;
						for (in = 0, ty = lista_objetos[i].y / TILE_SIZE; in < (lista_objetos[i].largo / TILE_SIZE); in++, ty++) map[(tx + 1) + (ty*SCENE_WIDTH)] = 3;
						//for (in = 0; in < (lista_objetos[i].largo / TILE_SIZE); in++, ty++) map[(tx + 2) + (ty*SCENE_WIDTH)] = 3;
					}
					else if (*llave == 3) {
						*llave = 1;
						Sonido.Play(SOUND_STAGE);
						lista_objetos[i].type = t_reemplazo;
						for (in = 0, ty = lista_objetos[i].y / TILE_SIZE; in < (lista_objetos[i].largo / TILE_SIZE); in++, ty++) map[tx + (ty*SCENE_WIDTH)] = 3;
						for (in = 0, ty = lista_objetos[i].y / TILE_SIZE; in < (lista_objetos[i].largo / TILE_SIZE); in++, ty++) map[(tx + 1) + (ty*SCENE_WIDTH)] = 3;
						//for (in = 0; in < (lista_objetos[i].largo / TILE_SIZE); in++, ty++) map[(tx + 2) + (ty*SCENE_WIDTH)] = 3;
					}
					else lista_objetos[i].active = true;
				}
				if (lista_objetos[i].type == t_reemplazo) lista_objetos[i].active = true;
				else return lista_objetos[i].type;
			}
		}
	}
}

void cObjetos::Reset()
{
	int i;
	for (i = 0; i < size_objeto; i++)
	{
		lista_objetos[i].x = 0;
		lista_objetos[i].y = 0;
		lista_objetos[i].active = false;
		lista_objetos[i].count = 0;
		lista_objetos[i].ancho = 0;
		lista_objetos[i].largo = 0;
		lista_objetos[i].type = 0;
		lista_objetos[i].type_al_romper = 0;
	}
}

int cObjetos::TOb()
{
	int i, c;
	for (i = 0, c=0; i < size_objeto; i++){
		if (lista_objetos[i].active) c++;
	}
	return c;
}