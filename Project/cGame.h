#pragma once

#include "cScene.h"
#include "cPlayer.h"
#include "cData.h"
#include "cObjetos.h"
#include "cEnemigo.h"
#include "cSound.h"

#define GAME_WIDTH	640
#define GAME_HEIGHT 480

class cGame
{
public:
	cGame(void);
	virtual ~cGame(void);

	bool Init();
	bool Loop(int f);
	void Finalize();
	//Input
	void ReadKeyboard(unsigned char key, int x, int y, bool press);
	void ReadMouse(int button, int state, int x, int y);
	//Process
	bool Process();
	//Output
	void Render();
	void render_info();
	void render_string(void* font, const char* string);
	void bu();
	bool render_mainmenu();
	void Parpadeo();
	bool LevelInit(int k, int lives);
	void DeadTime();
	void PauseTime();
	bool EndLevel();

private:
	unsigned char keys[256];
	int posx_initlevel[2];
	int posy_initlevel[2];
	bool game_init;
	int cursor;
	int cursorpause;
	int last_state;
	int pantalla_actual;
	int level_actual;
	float puntos;
	cScene Scene;
	cPlayer Player;
	cData Data;
	cObjetos Objetos;
	cEnemigo Enemigos;
	cSound Sonido;
};
