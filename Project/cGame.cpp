#include "cGame.h"
#include "Globals.h"
#include "cObjetos.h"

double camx, camy;
unsigned int last_move_cursor = 0;
int t_dead = 0;
unsigned int last_move_cursor_pause = 0;
cGame::cGame(void)
{
	game_init = false;
	cursor = 0;
	pantalla_actual = 1;
	cursorpause = 0;
	level_actual = 1;
	posx_initlevel[0] = 3;
	posy_initlevel[0] = 92;
	//posx_initlevel[0] = 90;
	//posy_initlevel[0] = 3;
	posx_initlevel[1] = 4;
	posy_initlevel[1] = 92;
	//posx_initlevel[1] = 32;
	//posy_initlevel[1] = 16;
	puntos = 9999.0f;
}

cGame::~cGame(void)
{
}

bool cGame::Init()
{
	bool res=true;

	Sonido.Load();

	//Graphics initialization
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,GAME_WIDTH,0,GAME_HEIGHT,0,1);
	glMatrixMode(GL_MODELVIEW);
	
	glAlphaFunc(GL_GREATER, 0.05f);
	glEnable(GL_ALPHA_TEST);
	
	//Scene initialization
	res = Data.LoadImage(IMG_BLOCKS,"Images/blocks01.png",GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_PLAYER, "Images/bub.png", GL_RGBA);
	if(!res) return false;
	res = Data.LoadImage(IMG_CRUZ, "Images/cruz.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(IMG_IRONMAN, "Images/ironman.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(IMG_GOHAN, "Images/gohan.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(IMG_CARA, "Images/bub_caras.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(IMG_JESUCRISTO, "Images/ui.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(IMG_HPBAR, "Images/hpbar.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(iMG_NUMEROS, "Images/numeros.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(IMG_OBJETOS, "Images/objetos.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(IMG_MAINMENU, "Images/mainmenu.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(IMG_CREDITOS, "Images/creditos.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(IMG_INSTRUCCIONES, "Images/instrucciones.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(IMG_ZUBAT, "Images/zubat.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(IMG_DEAD, "Images/dead.png", GL_RGBA);
	if (!res) return false;
	res = Data.LoadImage(IMG_PAUSE, "Images/pause.png", GL_RGBA);
	if (!res) return false;

	Sonido.Play(SOUND_MENU);
	return res;
}

bool cGame::LevelInit(int k, int lives)
{
	Sonido.StopAll();
	bool res;
	if (k == 1){
		Sonido.Play(SOUND_AMBIENT1);
		res = Data.LoadImage(IMG_BLOCKS, "Images/blocks01.png", GL_RGBA);
		if (!res) return false;
	}

	else {
		res = Data.LoadImage(IMG_BLOCKS, "Images/blocks02.png", GL_RGBA);
		if (!res) return false;
		Sonido.Play(SOUND_AMBIENT2);
	}

	//Graphics initialization
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, GAME_WIDTH, 0, GAME_HEIGHT, 0, 1);
	glMatrixMode(GL_MODELVIEW);

	glAlphaFunc(GL_GREATER, 0.05f);
	glEnable(GL_ALPHA_TEST);

	Objetos.Reset();
	Enemigos.Reset();


	res = Scene.LoadLevel(k);
	if (!res) return false;
	res = Objetos.LoadItems(k);
	if(!res) return false;
	res = Enemigos.LoadEnemigos(k);
	if (!res) return false;

	/*Reset Player status*/
	Player.SetWidthHeight(32,64);
	Player.SetHp(100);
	Player.SetLlaves(0);
	Player.SetSupalatigo(false);
	Player.SetTurbo(0.0f);
	Player.SetLives(lives);
	Player.SetDeadAct(0);
	Player.SetTile(posx_initlevel[k-1], posy_initlevel[k-1]);
	Player.SetWidthHeight(32,64);
	Player.SetState(STATE_LOOKRIGHT);

	return res;
}

void cGame::DeadTime()
{
	if ((Player.DeadAct() <= 2) & ((t_dead + 1000) < glutGet(GLUT_ELAPSED_TIME))) {
		Player.SetDeadAct(Player.DeadAct() + 1);
		t_dead = glutGet(GLUT_ELAPSED_TIME);
	}
	Player.SetWidthHeight(128, 128);
	Player.SetPosition(camx + ((GAME_WIDTH - 128) / 2) - 64, camy + ((GAME_HEIGHT-128)/2));
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();
	Scene.Draw(Data.GetID(IMG_BLOCKS));
	Scene.DrawDead(camx, camy);
	Player.Draw(Data.GetID(IMG_PLAYER));
	//Scene.DrawInterfaz(Player.GetHp(), Player.GetTurbo(), camx, camy, Player.GetThrowa(), Player.GetLives(), 0, Player.GetTurbo(), 2, Player.GetLlaves(), Player.GetSupalatigo());
	glutSwapBuffers();

	if (keys[32] & Player.DeadAct() >= 3) {
		if(Player.GetLives() > 0) LevelInit(1, Player.GetLives() - 1);
		else {
			game_init = false;
			Sonido.StopAll();
			Sonido.Play(SOUND_MENU);
		}
		last_move_cursor = glutGet(GLUT_ELAPSED_TIME) + 750;
	}
}

void cGame::PauseTime()
{

	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();
	Scene.DrawGuarro(16, 0.0f, 1.0f, 1.0f, 0.0f, camx + 200, camy + 160, 240, 160);
	Scene.DrawGuarro(10, 0.1f, 0.6f, 0.2f, 0.5f, camx + 200, camy + 248 - (cursorpause * 48), 32, 32);
	//Scene.DrawInterfaz(Player.GetHp(), Player.GetTurbo(), camx, camy, Player.GetThrowa(), Player.GetLives(), 0, Player.GetTurbo(), 2, Player.GetLlaves(), Player.GetSupalatigo());
	glutSwapBuffers();

	if (keys[13] & glutGet(GLUT_ELAPSED_TIME) > (last_move_cursor_pause + 500)) {
		Sonido.Play(SOUND_ENTRY);
		if (cursorpause == 0){
			Player.SetState(last_state);
			if (level_actual == 1){

				Sonido.Play(SOUND_AMBIENT1);
			}

			else{
				Sonido.Play(SOUND_AMBIENT2);
			}
		}
		else{
			game_init = false;

			Sonido.Play(SOUND_MENU);
		}
		
		last_move_cursor = glutGet(GLUT_ELAPSED_TIME);
		last_move_cursor_pause = glutGet(GLUT_ELAPSED_TIME);
	}
	if (keys[GLUT_KEY_DOWN] & glutGet(GLUT_ELAPSED_TIME) > (last_move_cursor_pause + 500)) {
		cursorpause -= 1;
		last_move_cursor_pause = glutGet(GLUT_ELAPSED_TIME);
		Sonido.Play(SOUND_SELECT);
	}
	if (keys[GLUT_KEY_UP] & glutGet(GLUT_ELAPSED_TIME) > (last_move_cursor_pause + 500)) {
		cursorpause += 1;
		last_move_cursor_pause = glutGet(GLUT_ELAPSED_TIME);
		Sonido.Play(SOUND_SELECT);
	}
	if (cursorpause > 1) cursorpause = 0;
	else if (cursorpause < 0) cursorpause = 1;
}

int fpscGame;
bool cGame::Loop(int f)
{
	bool res = true;
	fpscGame = f;
	Sonido.Update();

	if (game_init){
		if (Player.GetState() != STATE_DEAD & Player.GetState() != STATE_PAUSE & Player.GetState() != STATE_END_LEVEL) {
			res = Process();
			if (res & Player.GetState() != STATE_PAUSE) Render();
		}
		else if (Player.GetState() == STATE_PAUSE) {
			PauseTime();
		}
		else if (Player.GetState() == STATE_END_LEVEL) {
			switch (level_actual)
			{
			case 1:
				LevelInit(2, Player.GetLives());
				level_actual = 2;
				break;
			case 2:
				game_init = false;
				Sonido.StopAll();
				Sonido.Play(SOUND_MENU);
				pantalla_actual = 3;
				break;
			}
		}
		else {
			DeadTime();
		}
	}
	else {
		res = render_mainmenu();
	}
	return res;
}

bool cGame::render_mainmenu()
{
	bool res = true;


	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, GAME_WIDTH, 0, GAME_HEIGHT, 0, 1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (pantalla_actual == 1) {
		Scene.DrawGuarro(11, 0.0f, 1.0f, 1.0f, 0.0f, 0, 0, 640, 480);
		Scene.DrawGuarro(10, 0.1f, 0.6f, 0.2f, 0.5f, 3.5 * TILE_SIZE, (5 * TILE_SIZE) - (cursor*TILE_SIZE * 2), 32, 32);
		if (keys[27]) res = false;
		if (keys[GLUT_KEY_DOWN] & ((last_move_cursor + 250) < glutGet(GLUT_ELAPSED_TIME))) {
			cursor += 1;
			if (cursor >= 3) cursor = 0;
			last_move_cursor = glutGet(GLUT_ELAPSED_TIME);
			Sonido.Play(SOUND_SELECT);
		}
		if (keys[GLUT_KEY_UP] & ((last_move_cursor + 250) < glutGet(GLUT_ELAPSED_TIME))) {
			cursor -= 1;
			if (cursor < 0) cursor = 2;
			last_move_cursor = glutGet(GLUT_ELAPSED_TIME);
			Sonido.Play(SOUND_SELECT);
		}
		if (keys[13] & ((last_move_cursor + 250) < glutGet(GLUT_ELAPSED_TIME))) {
			last_move_cursor = glutGet(GLUT_ELAPSED_TIME);
			Sonido.Play(SOUND_ENTRY);

			Parpadeo();

			if (cursor == 0) {
				game_init = true;
				LevelInit(1, 1);
				level_actual = 1;
			}
			if (cursor == 1) pantalla_actual = 2;
			if (cursor == 2) pantalla_actual = 3;
			
		}
	}
	if (pantalla_actual == 2) {
		Scene.DrawGuarro(13, 0.0f, 1.0f, 1.0f, 0.0f, 0, 0, 640, 480);
		if (keys[13] & ((last_move_cursor + 250) < glutGet(GLUT_ELAPSED_TIME))) {
			pantalla_actual = 1;
			last_move_cursor = glutGet(GLUT_ELAPSED_TIME);
		}
	}
	if (pantalla_actual == 3) {
		Scene.DrawGuarro(12, 0.0f, 1.0f, 1.0f, 0.0f, 0, 0, 640, 480);
		if (keys[13] & ((last_move_cursor + 250) < glutGet(GLUT_ELAPSED_TIME))) {
			pantalla_actual = 1;
			last_move_cursor = glutGet(GLUT_ELAPSED_TIME);
		}
	}
	glutSwapBuffers();
	return res;
}

void cGame::Parpadeo()
{
	int parpadeo = 1;
	int last_p = glutGet(GLUT_ELAPSED_TIME);
	while (((last_move_cursor + 2000) > glutGet(GLUT_ELAPSED_TIME))) {
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, GAME_WIDTH, 0, GAME_HEIGHT, 0, 1);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		Scene.DrawGuarro(10, 0.1f + (parpadeo % 2)*0.1f, 0.6f, 0.2f + (parpadeo % 2)*0.1f, 0.5f, 3.5 * TILE_SIZE, (5 * TILE_SIZE) - (cursor*TILE_SIZE * 2), 32, 32);
		if ((last_p + 250) < glutGet(GLUT_ELAPSED_TIME)) {
			parpadeo += 1;
			last_p = glutGet(GLUT_ELAPSED_TIME);
		}
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glutSwapBuffers();
	}
	last_move_cursor = glutGet(GLUT_ELAPSED_TIME);
}

void cGame::Finalize()
{
}

//Input
void cGame::ReadKeyboard(unsigned char key, int x, int y, bool press)
{
	keys[key] = press;
}

void cGame::ReadMouse(int button, int state, int x, int y)
{
}

bool primer = true;
bool second = true;
bool primer_attack = true;
bool primer_throw = true;
unsigned int t = 99999999999999999;
//Process
bool cGame::Process()
{

	int xmia, ymia, w, h, st;
	Player.GetPosition(&xmia, &ymia);
	st = Player.GetState();
	Player.GetWidthHeight(&w, &h);
	if (primer) {
		//Enemigos.AddEnemigo(22*32, 90*32, Enemy_Ironman, 32, 64, false);
		//Enemigos.AddEnemigo(32* 10, 94 * 32, Enemy_Ironman, 32, 64, true);
		//Enemigos.AddEnemigo(32 * 7, 93 * 32, Enemy_zubat, 32, 32, true);
		//Enemigos.AddGea(32 * 11, 87 * 32, true);
		//Enemigos.AddGea(32 * 8, 90 * 32, true);
		//Objetos.AddObjeto(320, 160, 32, 32, t_velita);
		//Enemigos.AddEnemigo(192, 64, Enemy_Togemon, 32, 64, false);
		//Enemigos.AddEnemigo(128, 300, Enemy_Wargreymon, 48, 64, false);
		//Enemigos.AddEnemigo(250, 64, Enemy_gohan, 32, 64, true);
		//Enemigos.AddEnemigo(128, 300, Enemy_gohan, 32, 64, false);
		/*Objetos.AddObjeto(128, 160, 32, 32, t_cofre);
		Objetos.AddObjeto(160, 160, 32, 32, t_llavecofre);
		Objetos.AddObjeto(192, 160, 32, 32, t_llavepuerta);
		Objetos.AddObjeto(224, 160, 32, 32, t_vida);
		Objetos.AddObjeto(256, 160, 32, 32, t_hp);
		Objetos.AddObjeto(288, 160, 32, 32, t_fuego);
		Objetos.AddObjeto(320, 160, 32, 32, t_velita);*/
		primer = false;
	}
	bool res=true;
	
	//Process Input
	if (keys[27])	{
		last_state = Player.GetState();
		Sonido.StopAll();
		Sonido.Play(SOUND_PAUSE);
		Player.SetState(STATE_PAUSE);
		cursorpause = 0;
		return true;
	}
	if (keys[GLUT_KEY_DOWN] & (st != STATE_ATTACKCROUCHLEFT & st!= STATE_ATTACKCROUCHRIGHT & st != STATE_ATTACKLEFT & st != STATE_ATTACKRIGHT & st != STATE_ATTACKESCALERALEFT & st != STATE_ATTACKESCALERARIGHT & st != STATE_ATTACKESCALERALEFTBOT & st != STATE_ATTACKESCALERARIGHTBOT)) {
		bool bdw1 = false; bool bdw2 = false;
		if (Player.GetJump()) Player.Jump(Scene.GetMap());
		else if (Player.GetEscalera() == true) {
			Player.MoveEscalera(Scene.GetMap(), false); //bajar
		}
		else if (Player.CollidesMapFloor(Scene.GetMap())) {
			bdw1 = (Player.CollidesMapEscaleraBot(Scene.GetMap(), false));
			if (!bdw1) bdw2 = (Player.CollidesMapEscaleraBot(Scene.GetMap(), true));
		}
		if (!bdw2 & ! bdw1) { 
			int st = Player.GetState();
			if (st == STATE_LOOKLEFT | st == STATE_WALKLEFT) Player.SetState(STATE_CROUCHLEFT);
			else if (st == STATE_LOOKRIGHT | st == STATE_WALKRIGHT) Player.SetState(STATE_CROUCHRIGHT);
			//return res;
		}
	}
	else if (keys[GLUT_KEY_LEFT] & (st != STATE_ATTACKESCALERALEFT & st != STATE_ATTACKESCALERARIGHT & st != STATE_ATTACKESCALERALEFTBOT & st != STATE_ATTACKESCALERARIGHTBOT))	{
		Player.SetSubiendo(false);
		if(st != STATE_ATTACKCROUCHLEFT & st != STATE_ATTACKCROUCHRIGHT) Player.MoveLeft(Scene.GetMap());
	}
	else if (keys[GLUT_KEY_RIGHT] & (st != STATE_ATTACKESCALERALEFT & st != STATE_ATTACKESCALERARIGHT & st != STATE_ATTACKESCALERALEFTBOT & st != STATE_ATTACKESCALERARIGHTBOT))	{
		Player.SetSubiendo(false);
		if (st != STATE_ATTACKCROUCHLEFT & st != STATE_ATTACKCROUCHRIGHT) Player.MoveRight(Scene.GetMap());
	}
	else Player.Stop();

	if (Player.ThrowAct() >= 1) {
		if (st == STATE_THROWRIGHT) Player.Throw(true);
		else if (st == STATE_THROWLEFT) Player.Throw(false);
		if (Player.ThrowAct() == 3) {
			if (primer_throw) {
				int xmia, ymia;
				Player.GetPosition(&xmia, &ymia);
				if(st == STATE_THROWRIGHT) Player.SetPosition(xmia + 32, ymia);
				else if(st == STATE_THROWLEFT) Player.SetPosition(xmia - 32, ymia);
				primer_throw = false;
			}
		}
		if (Player.ThrowAct() >= 4) {
			Player.SetThrowAct(0);
			int xmia, ymia;
			Player.GetPosition(&xmia, &ymia);
			if (st == STATE_THROWRIGHT) {
				Player.SetState(STATE_LOOKRIGHT);
				if (Player.GetTurbo() == 100.0f) {
					Sonido.Play(SOUND_BOLA);
					Objetos.AddCruz(xmia + TILE_SIZE, ymia + (TILE_SIZE), true, true);
					Player.SetTurbo(0.0f);
				}
				else Objetos.AddCruz(xmia + TILE_SIZE + (TILE_SIZE/2), ymia + (TILE_SIZE), true, false);
				//Player.SetPosition(xmia + 32, ymia);
			}
			else if (st == STATE_THROWLEFT) {
				Player.SetState(STATE_LOOKLEFT);
				if (Player.GetTurbo() == 100.0f) {
					Sonido.Play(SOUND_BOLA);
					Objetos.AddCruz(xmia - 2 * TILE_SIZE, ymia + (TILE_SIZE), false, true);
					Player.SetTurbo(0.0f);
				}
				else Objetos.AddCruz(xmia /*+ 2*TILE_SIZE*/, ymia + (TILE_SIZE), false, false);
				Player.SetPosition(xmia + 32, ymia);
			}
			Player.SetWidthHeight(32, 64);
			Player.Logic(Scene.GetMap());
		}
		else {
			Player.Logic(Scene.GetMap());
			return true;
		}
	}
	else if (Player.AttackAct() >= 1) {
		if (st == STATE_ATTACKRIGHT) Player.Attack(true);
		else if (st == STATE_ATTACKLEFT) Player.Attack(false);
		else if (st == STATE_ATTACKCROUCHRIGHT) Player.Attack(true);
		else if (st == STATE_ATTACKCROUCHLEFT) Player.Attack(false);
		else if (st == STATE_ATTACKESCALERARIGHT) Player.Attack(true);
		else if (st == STATE_ATTACKESCALERALEFT) Player.Attack(false);
		else if (st == STATE_ATTACKESCALERARIGHTBOT) Player.Attack(true);
		else if (st == STATE_ATTACKESCALERALEFTBOT) Player.Attack(false);
		if (Player.AttackAct() == 3) {
			int xmia, ymia;
			Player.GetPosition(&xmia, &ymia);
			if (primer_attack) {
				Sonido.Play(SOUND_LATIGO);
				Player.SetWidthHeight(96, 64);
				if (st == STATE_ATTACKLEFT){
					Player.SetPosition(xmia - 64, ymia);
					Objetos.CollidesAttack(xmia - 64, ymia + 32, 64, 16, Scene.GetMap());
					Enemigos.CollidesAttack(xmia - 64, ymia + 32, 64, 16, Player.GetDaņoLatigo());
				}
				else if (st == STATE_ATTACKCROUCHLEFT) {
					Player.SetPosition(xmia - 64, ymia);
					Objetos.CollidesAttack(xmia - 64, ymia + 16, 64, 16, Scene.GetMap());
					Enemigos.CollidesAttack(xmia - 64, ymia + 16, 64, 16, Player.GetDaņoLatigo());
				}
				else if (st == STATE_ATTACKESCALERALEFT) {
					Player.SetPosition(xmia - 64, ymia);
					Objetos.CollidesAttack(xmia - 64, ymia + 32, 64, 16, Scene.GetMap());
					Enemigos.CollidesAttack(xmia - 64, ymia + 32, 64, 16, Player.GetDaņoLatigo());
				}
				else if (st == STATE_ATTACKESCALERALEFTBOT) {
					Player.SetPosition(xmia - 64, ymia);
					Objetos.CollidesAttack(xmia - 64, ymia + 32, 64, 16, Scene.GetMap());
					Enemigos.CollidesAttack(xmia - 64, ymia + 32, 64, 16, Player.GetDaņoLatigo());
				}
				else if (st == STATE_ATTACKRIGHT) {
					Player.SetPosition(xmia + 32, ymia);
					Objetos.CollidesAttack(xmia + 64, ymia + 32, 64, 16, Scene.GetMap());
					Enemigos.CollidesAttack(xmia + 64, ymia + 32, 64, 16, Player.GetDaņoLatigo());
				}
				else if (st == STATE_ATTACKCROUCHRIGHT) {
					Player.SetPosition(xmia + 32, ymia);
					Objetos.CollidesAttack(xmia + 64, ymia + 16, 64, 16, Scene.GetMap());
					Enemigos.CollidesAttack(xmia + 64, ymia + 16, 64, 16, Player.GetDaņoLatigo());
				}
				else if (st == STATE_ATTACKESCALERARIGHT) {
					Player.SetPosition(xmia + 32, ymia);
					Objetos.CollidesAttack(xmia + 64, ymia + 32, 64, 16, Scene.GetMap());
					Enemigos.CollidesAttack(xmia + 64, ymia + 32, 64, 16, Player.GetDaņoLatigo());
				}
				else if (st == STATE_ATTACKESCALERARIGHTBOT) {
					Player.SetPosition(xmia + 32, ymia);
					Objetos.CollidesAttack(xmia + 64, ymia + 32, 64, 16, Scene.GetMap());
					Enemigos.CollidesAttack(xmia + 64, ymia + 32, 64, 16, Player.GetDaņoLatigo());
				}
				primer_attack = false;
			}
		}
		if (Player.AttackAct() >= 4) {
			Player.SetAttackAct(0);
			int xmia, ymia;
			Player.GetPosition(&xmia, &ymia);
			if (st == STATE_ATTACKRIGHT) {
				Player.SetState(STATE_LOOKRIGHT);
			}
			else if (st == STATE_ATTACKLEFT) {
				Player.SetState(STATE_LOOKLEFT);
			}
			else if (st == STATE_ATTACKCROUCHRIGHT) {
				Player.SetState(STATE_CROUCHRIGHT);
			}
			else if (st == STATE_ATTACKCROUCHLEFT) {
				Player.SetState(STATE_CROUCHLEFT);
			}
			else if (st == STATE_ATTACKESCALERARIGHT) {
				Player.SetState(STATE_WAITINGESCALERARIGHT);
			}
			else if (st == STATE_ATTACKESCALERALEFT) {
				Player.SetState(STATE_WAITINGESCALERALEFT);
			}
			else if (st == STATE_ATTACKESCALERARIGHTBOT) {
				Player.SetState(STATE_WAITINGESCALERARIGHTBOT);
			}
			else if (st == STATE_ATTACKESCALERALEFTBOT) {
				Player.SetState(STATE_WAITINGESCALERALEFTBOT);
			}
			Player.SetWidthHeight(32, 64);
			if (st == STATE_ATTACKLEFT | st == STATE_ATTACKCROUCHLEFT | st == STATE_ATTACKESCALERALEFT | st == STATE_ATTACKESCALERALEFTBOT) Player.SetPosition(xmia + 64, ymia);
			Player.Logic(Scene.GetMap());
		}
		else {
			Player.Logic(Scene.GetMap());
			return true;
		}
	}

	if (keys[32]) { //1
		if (st == STATE_LOOKRIGHT | st == STATE_WALKRIGHT | st == STATE_CROUCHRIGHT | st == STATE_WAITINGESCALERARIGHT | st == STATE_WAITINGESCALERARIGHTBOT) {
			int xmia, ymia;
			Player.GetPosition(&xmia, &ymia);
			Player.SetWidthHeight(64, 64);
			Player.SetPosition(xmia - 32, ymia);
			Player.Attack(true);
		}
		else if (st == STATE_LOOKLEFT | st == STATE_WALKLEFT | st == STATE_CROUCHLEFT | st == STATE_WAITINGESCALERALEFT | st == STATE_WAITINGESCALERALEFTBOT) {
			int xmia, ymia;
			Player.GetPosition(&xmia, &ymia);
			Player.SetWidthHeight(64, 64);
			Player.SetPosition(xmia, ymia);
			Player.Attack(false);
		}
		Player.Logic(Scene.GetMap());
		primer_attack = true;
		return true;
	}
	/*if (keys[50]){ //2
		//Enemigos.AddEnemigo(xmia, ymia, Enemy_Wargreymon, 48, 64, false);
		//Player.SetDamaged(3000);
		Player.SetHp(Player.GetHp() - 1);
		//t = 0;
	}*/
	/*if (keys[51]){ //35
		if (second) {
			//Objetos.LoadItems(1);
			//Enemigos.AddEnemigo(xmia + (32 * 6), ymia + (2*32), Enemy_Ironman, 5, true);
			Player.SetLlaves(3);
			Player.SetHp(100);
			//Enemigos.AddEnemigo(32 * 7, 93 * 32, Enemy_zubat, 32, 32, false);
			//Objetos.Reset();
			//Objetos.LoadItems(1);
			second = false;
		}
		//Player.SetDamaged(300);
	}*/
	/*if (keys[52]) { //4
		second = true;
	}
	if (keys[54]) { //
		if (second) {
			Objetos.Reset();
			Objetos.LoadItems(2);
			Enemigos.Reset();
			Enemigos.LoadEnemigos(2);
			second = false;
		}
	}*/
	if (keys[122] | keys[90]) { //z
		bool puc_throw;
		if (st == STATE_LOOKRIGHT | st == STATE_WALKRIGHT | st == STATE_CROUCHRIGHT) {
			puc_throw = Player.Throw(true);
			if (puc_throw) {
				int xmia, ymia;
				Player.GetPosition(&xmia, &ymia);
				Player.SetWidthHeight(64, 64);
				Player.SetPosition(xmia - 32, ymia);
				Player.Logic(Scene.GetMap());
				primer_throw = true;
				Sonido.Play(SOUND_THROW);
				return true;
			}	
		}
		else if (st == STATE_LOOKLEFT | st == STATE_WALKLEFT | st == STATE_CROUCHRIGHT) {
			puc_throw = Player.Throw(false);
			if (puc_throw) {
				int xmia, ymia;
				Player.GetPosition(&xmia, &ymia);
				Player.SetWidthHeight(64, 64);
				Player.SetPosition(xmia, ymia);
				Player.Logic(Scene.GetMap());
				primer_throw = true;
				Sonido.Play(SOUND_THROW);
				return true;
			}
		}
	}
	//else Player.Stop();
	
	if (Player.SubiendoAct() > 1) {
		int st = Player.GetState();
		if (st == STATE_ESCALERARIGHT | st == STATE_ESCALERALEFT |st == STATE_WAITINGESCALERALEFT | st == STATE_WAITINGESCALERARIGHT) Player.MoveEscalera(Scene.GetMap(), true);
		else if (st == STATE_ESCALERALEFTBOT | st == STATE_ESCALERARIGHTBOT | st == STATE_WAITINGESCALERALEFTBOT | st == STATE_WAITINGESCALERARIGHTBOT) Player.MoveEscalera(Scene.GetMap(), false);
	}
	if (keys[GLUT_KEY_UP] & !keys[GLUT_KEY_DOWN]) {
		bool bup1 = false; bool bup2 = false;
		if (Player.GetJump()) Player.Jump(Scene.GetMap());
		else if (Player.GetEscalera() == true) {
			Player.MoveEscalera(Scene.GetMap(), true); //subir
		}
		else if (Player.CollidesMapFloor(Scene.GetMap())) {
			bup1 = (Player.CollidesMapEscalera(Scene.GetMap(), false));
			if(!bup1) bup2 = (Player.CollidesMapEscalera(Scene.GetMap(), true));
		}
		if(!bup2 & !bup1) Player.Jump(Scene.GetMap());
	}
	
	//Game Logic
	if(!Player.GetEscalera()) Player.Logic(Scene.GetMap());

	return res;
}

void cGame::bu()
{	
		int mean, h1, xmia, ymia;
		int at = Player.AttackAct();
		int tt = Player.ThrowAct();
		int st = Player.GetState();
		Player.GetPosition(&xmia, &ymia);

			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			mean = ymia;
			double visible_areabottom = mean - (GAME_HEIGHT / 2) + TILE_SIZE;

			double visible_arealeft = xmia - (GAME_WIDTH/2) + SCENE_Xo + (32 / 2);
			if (at == 3 & (st == STATE_ATTACKCROUCHLEFT | st == STATE_ATTACKLEFT | st == STATE_ATTACKESCALERALEFT | st == STATE_ATTACKESCALERALEFTBOT)) visible_arealeft += 64;
			if (at <= 2 & (st == STATE_ATTACKCROUCHRIGHT | st == STATE_ATTACKRIGHT | st == STATE_ATTACKESCALERARIGHT | st == STATE_ATTACKESCALERARIGHTBOT)) visible_arealeft += 32;
			if (tt <= 2 & st == STATE_THROWRIGHT)  visible_arealeft += 32;
			if (tt == 3 & st == STATE_THROWLEFT)  visible_arealeft += 32;
			if (visible_arealeft < 64) visible_arealeft = 64;
			if (visible_areabottom < 0) visible_areabottom = 0;
			double visible_arearight = visible_arealeft + GAME_WIDTH;
			double visible_areatop = visible_areabottom + GAME_HEIGHT;
			camx = visible_arealeft;
			camy = visible_areabottom;
			glOrtho(visible_arealeft, visible_arearight, visible_areabottom, visible_areatop, 3, -SCENE_HEIGHT*TILE_SIZE - 2 - 1);
			//Overlay1.SetY(visible_area.bottom);   //overlays se mueven junto a la camara
			//Overlay2.SetY(visible_area.bottom);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			//Scene.DrawGuarro(11, 0.0f, 1.0f, 1.0f, 0.0f, visible_arealeft, visible_areabottom, 640, 480);
}

//Output
void cGame::Render()
{
	int xmia, ymia, g, w, h, st;
	int collene = 0;
	st = Player.GetState();
	int hp_, lives_, throwa_, llave_;
	bool supalatigo_;
	Player.GetPosition(&xmia, &ymia);
	Player.GetWidthHeight(&w, &h);
	if (st == STATE_ATTACKCROUCHRIGHT | st == STATE_ATTACKCROUCHLEFT | st == STATE_CROUCHLEFT | st == STATE_CROUCHRIGHT) h = (h * 3) / 4;
	llave_ = Player.GetLlaves();
	glClear(GL_COLOR_BUFFER_BIT);
	
	glLoadIdentity();

	Scene.Draw(Data.GetID(IMG_BLOCKS));
	
	Objetos.CollidesThrows(Scene.GetMap());
	if (st == STATE_ATTACKRIGHT | st == STATE_ATTACKCROUCHRIGHT | st == STATE_ATTACKESCALERARIGHT | st == STATE_ATTACKESCALERARIGHTBOT) {
		if (Player.AttackAct() <= 2) {
			xmia += TILE_SIZE;
		}
	}
	else if (st == STATE_ATTACKLEFT | st == STATE_ATTACKCROUCHLEFT | st == STATE_ATTACKESCALERALEFT | st == STATE_ATTACKESCALERALEFTBOT) {
		if (Player.AttackAct() == 3) {
			xmia += 2 * TILE_SIZE;
		}
	}
	if (st == STATE_THROWRIGHT) {
		if (Player.ThrowAct() <= 2) {
			xmia += TILE_SIZE;
		}
	}
	else if (st == STATE_THROWLEFT) {
		if (Player.ThrowAct() == 3) {
			xmia += TILE_SIZE;
		}
	}

	supalatigo_ = Player.GetSupalatigo();
	g = Objetos.CollidesHeroe(Scene.GetMap(), xmia, ymia, TILE_SIZE, h, &hp_, &lives_, &throwa_, &llave_, &supalatigo_);
	Enemigos.CollidesEnergy(Objetos.GetListaCruces(), Player.GetDaņoEnergy());
	Enemigos.RespawnEnemies(camx, camy);
	Player.SetHp(Player.GetHp() + hp_);
	Player.SetLives(Player.GetLives() + lives_);
	Player.SetThrowa(Player.GetThrowa() + throwa_);
	Player.SetLlaves(llave_);
	Player.SetSupalatigo(supalatigo_);
	if (Player.GetDamaged() <= 0) {
		collene = Enemigos.CollidesHero(xmia, ymia, TILE_SIZE, h);
		if (collene > 0) {
			Player.SetDamaged(3 * 60);
			Player.SetHp(Player.GetHp() - collene);
		}
	}
	Objetos.Draw(Data.GetID(IMG_CRUZ), level_actual);
	Player.Draw(Data.GetID(IMG_PLAYER));
	Enemigos.Draw(Scene.GetMap(), xmia, ymia);

	bu();
	if (Player.GetHp() <= 0) {
		Sonido.StopAll();
		Sonido.Play(SOUND_MUERTE);
		Player.SetState(STATE_DEAD);
		Player.SetDeadAct(1);
		t_dead = glutGet(GLUT_ELAPSED_TIME);
		Player.SetDamaged(0);
		Player.SetJumping(false);
	}
	bool aux = EndLevel();
	if (aux) Player.SetState(STATE_END_LEVEL);
	//render_mainmenu();
	Scene.DrawInterfaz(Player.GetHp(), Player.GetTurbo(), camx, camy, Player.GetThrowa(), Player.GetLives(), level_actual, puntos-=0.1f, 2, Player.GetLlaves(), Player.GetSupalatigo());
	//render_info();
	glutSwapBuffers();
}

void cGame::render_info()
{/*
	int fx, x1, y1, w, h;
	char hp[5], x[5], y[5];
	char *s2[] = { " - 07" };
	char *s [] = { 
		"SCORE-0000000 STAGE-01 P-02",
		"PLAYER HP: ", 
		"ENEMY HP: ",
		"POSITION XY: ",
		"COLISION ENEMIGO: ",
		"STATE: "
	};
	itoa(Player.GetHp(), hp, 10);
	itoa(Player.GetState(), hp, 10);
	Player.GetPosition(&x1, &y1);
	Player.GetWidthHeight(&w, &h);
	//itoa(x1, x, 10); itoa(y1, y, 10);

	glDisable(GL_TEXTURE_2D);
	glPushMatrix();
	glScalef(2.0f, 2.0f, 1.0f);
	for (fx = 0; fx < 6; fx++)
	{
		glRasterPos2f(camx, camy - fx*12 + 300);
		render_string(GLUT_BITMAP_TIMES_ROMAN_24, s[fx]);
	}
	glPopMatrix();
	glEnable(GL_TEXTURE_2D);*/
}

void cGame::render_string(void* font, const char* string)
{
	int i, len = strlen(string);
	for (i = 0; i < len; i++) {
		glutBitmapCharacter(font, string[i]);
	}
}

bool cGame::EndLevel()
{
	bool fin = false;
	int tx, ty;
	Player.GetPosition(&tx, &ty);
	tx = tx / 32; ty = ty / 32;
	switch (level_actual)
	{
	case 1:
		if (tx == 96 | tx == 97) {
			if (ty == 3 | ty == 4) {
				fin = true;
			}
		}
		break;
	case 2:
		if (tx == 26 | tx == 27) {
			if (ty == 16 | ty == 17) {
				fin = true;
			}
		}
		break;
	}
	return fin;
}
