#pragma once

#include "cBicho.h"
#include "cObjetos.h"

#define dist_mov_ironman			5
#define count_gea					150	
#define speed_gea					3
#define size_gea					99
#define dist_mov_max_ironman		36*3
#define time_action_ironman			100
#define time_action_gohan			100
#define time_kame_gohan				5000
#define time_action_zubat			100
#define size_enemigos				99
#define Enemy_Ironman				1
#define Enemy_Gohan					2
#define Enemy_zubat					3
#define width_gea					24
#define height_gea					8
#define hp_zubat					3
#define hp_ironman					12
#define hp_gohan					8

#define	STATE_WALK1							50
#define STATE_WALK2							51
#define	STATE_WALK3							52
#define STATE_WALK4							53
#define	STATE_ATTACK1						54
#define STATE_ATTACK2						55
#define	STATE_ATTACK3						56
#define STATE_ATTACK4						57


class cEnemigo : public cBicho
{
public:
	cEnemigo();
	~cEnemigo();

	struct FGea
	{
		bool active;
		int x;
		int y;
		bool right;
		unsigned int count;
	};

	struct Enemigo
	{
		int hp;
		int type;
		bool right;
		int x, y;
		int xo, yo;
		int xprev, yprev;
		bool active;
		int state;
		int largo, ancho;
		int largoo, anchoo;
		unsigned int last_action;
		unsigned int mov_count;
		unsigned int mov_count_att;
		int respawn_time;
		int count_respawn;
	};

	cSound Sonido;

	int index_gea;
	int index_enemigo;
	void Draw(int *map, int xmia, int ymia);
	void SetWidthHeight(int w, int h, int id);
	//void Logic(int *map);
	void AddEnemigo(int px, int py, int ptype, int respawn, bool pright);
	void DrawRectEn(int tex_id, float xo, float yo, float xf, float yf, float scx, float scy, int ancho, int largo);
	void IA_ironman(int id, int *map, int xmia, int ymia);
	void IA_Gohan(int id, int *map, int xmia, int ymia);
	void IA_Zubat(int id);
	bool CollidesMapFloorEn(int *map, int id);
	void DrawRectGea(int tex_id, float xo, float yo, float xf, float yf, float scx, float scy);
	void AddGea(int x, int y, bool right);
	int CollidesHero(int x, int y, int w, int h);
	int CollidesGea(int x, int y, int w, int h);
	void CollidesAttack(int x, int y, int w, int h, int d);
	void CollidesEnergy(struct Objeto *list, int d);
	void Reset();
	void RespawnEnemies(int camx, int camy);
	bool LoadEnemigos(int level);
	Enemigo lista_enemigos[size_enemigos];
	FGea lista_geas[size_gea];
};
