#include "cScene.h"
#include "Globals.h"

void cScene::draw_number(int scn_x, int scn_y, int t, int l)
{
	float xo, xf, yo, yf;
	xo = 0.0f; xf = 1.0f;
	yo = 0.1f + (0.1f * l);
	yf = yo - 0.1f;
	glEnable(GL_TEXTURE_2D);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 9);
	glBegin(GL_QUADS);
	glTexCoord2f(xo, yo);	glVertex2i(scn_x, scn_y);
	glTexCoord2f(xf, yo);	glVertex2i(scn_x + t, scn_y);
	glTexCoord2f(xf, yf);	glVertex2i(scn_x + t, scn_y + t);
	glTexCoord2f(xo, yf);	glVertex2i(scn_x, scn_y + t);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}

cScene::cScene(void)
{
}

cScene::~cScene(void)
{
}

bool cScene::LoadLevel(int level)
{
	bool res;
	FILE *fd;
	char file[16];
	int i,j,px,py, a;
	char tile1;
	char tile2;
	float coordx_tile, coordy_tile;

	res=true;

	if(level<10) sprintf(file,"%s0%d%s",(char *)FILENAME,level,(char *)FILENAME_EXT);
	else		 sprintf(file,"%s%d%s",(char *)FILENAME,level,(char *)FILENAME_EXT);

	fd=fopen(file,"r");
	if(fd==NULL) return false;

	id_DL=glGenLists(1);
	glNewList(id_DL,GL_COMPILE);
		glBegin(GL_QUADS);
	
			for(j = (SCENE_HEIGHT - 1);j>=0;j--)
			{
				px=SCENE_Xo;
				py=SCENE_Yo+(j*TILE_SIZE);

				for(i=0;i<SCENE_WIDTH;i++)
				{
					fscanf(fd, "%c", &tile1);
					/**/
					int ind = (tile1 - 48);
					fscanf(fd, "%c", &tile1);
					if (tile1 != ',') {
						ind = (ind * 10);
						ind = ind + (tile1 - 48);
						fscanf(fd, "%c", &tile1);
					
					}

					if (ind == 8){
						map[(j*SCENE_WIDTH) + i] = 0;
					}

					else{
						map[(j*SCENE_WIDTH) + i] = ind;

						a = (ind - 1) % 8;
						coordx_tile = 0.0f + 0.125f * (a);
						a = (ind - 1) / 8;
						coordy_tile = 0.0f + 0.1f * (a);

						//BLOCK_SIZE = 32, FILE_SIZE = 256
						// 32 / 

						glTexCoord2f(coordx_tile, coordy_tile + 0.1f); glVertex2i(px, py);
						glTexCoord2f(coordx_tile + 0.125f, coordy_tile + 0.1f); glVertex2i(px + BLOCK_SIZE, py);
						glTexCoord2f(coordx_tile + 0.125f, coordy_tile); glVertex2i(px + BLOCK_SIZE, py + BLOCK_SIZE);
						glTexCoord2f(coordx_tile, coordy_tile);	glVertex2i(px, py + BLOCK_SIZE);
					}
					px+=TILE_SIZE; 
				}
				fscanf(fd,"%c",&tile1); //pass enter
			}

		glEnd();
	glEndList();

	fclose(fd);

	return res;
}

void cScene::Draw(int tex_id)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,tex_id);
	glCallList(id_DL);
	glDisable(GL_TEXTURE_2D);
}
int* cScene::GetMap()
{
	return map;
}

void cScene::DrawDead(double camx, double camy)
{
	float xo, yo, xf, yf;
	xo = 0.0f; xf = 1.0f;
	yo = 1.0f;
	yf = yo - 1.0f;

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 15);
	glBegin(GL_QUADS);
	glTexCoord2f(xo, yo);	glVertex2i(camx, camy);
	glTexCoord2f(xf, yo);	glVertex2i(camx + 640, camy);
	glTexCoord2f(xf, yf);	glVertex2i(camx + 640, camy + 480);
	glTexCoord2f(xo, yf);	glVertex2i(camx, camy + 480);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}

int ctdib = 0;
void cScene::DrawInterfaz(int hpp, int turb, double camx, double camy, int cant_throw, int lives, int stage, int points, int phase, int llave, bool supalatigo)
{
	int screen_x, screen_y;
	int i;
	float xo, yo, xf, yf;

	screen_x = camx;
	screen_y = camy + ((13) * TILE_SIZE);

	//i = hpp / 20;
	//if (hpp == 100) i = 99 / 20;
	xo = 0.0f; xf = 1.0f;
	yo = 1.0f;
	yf = yo - 1.0f;

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 7);
	glBegin(GL_QUADS);
	glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
	glTexCoord2f(xf, yo);	glVertex2i(screen_x + ((16 + 4)*TILE_SIZE), screen_y);
	glTexCoord2f(xf, yf);	glVertex2i(screen_x + ((16 + 4)*TILE_SIZE), screen_y + (2 * TILE_SIZE));
	glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + (2 * TILE_SIZE));
	glEnd();

	glDisable(GL_TEXTURE_2D);

	//drawcara
	screen_x = camx + SCENE_Xo;
	screen_y = camy + ((13) * TILE_SIZE);

	i = hpp / 20;
	if (hpp == 100) i = 99 / 20;
	xo = 0.0f; xf = 1.0f;
	if (turb == 100 & hpp > 0) yo = 0.875;
	else yo = 0.875 - 0.125*(i + 2);
	yf = yo - 0.125;

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 6);
	glBegin(GL_QUADS);
	glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
	glTexCoord2f(xf, yo);	glVertex2i(screen_x + 64, screen_y);
	glTexCoord2f(xf, yf);	glVertex2i(screen_x + 64, screen_y + 64);
	glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + 64);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	//draw hpbar
	screen_x = camx + (10.5f * TILE_SIZE) + 6;
	screen_y = camy + ((14 * TILE_SIZE) - ((TILE_SIZE * 3) / (TILE_SIZE / 2)));
	int k;
	int c;
	for (i = 100, c = 99; i >= 0; i--) { //hpheroe
		k = ((hpp) / c);
		if (k && (hpp >= 0)) xo = 0.0f;
		else xo = 0.5f;
		xf = xo + 0.5f;
		yo = 0.5f; yf = yo - 0.5f;
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 8);
		glBegin(GL_QUADS);
		glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
		glTexCoord2f(xf, yo);	glVertex2i(screen_x + 1, screen_y);
		glTexCoord2f(xf, yf);	glVertex2i(screen_x + 1, screen_y + 12);
		glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + 12);
		glEnd();
		glDisable(GL_TEXTURE_2D);
		c -= 1;
		if (c <= 0) c = 1;
		screen_x -= 1;
	}

	//draw turbobar
	screen_x = camx + (10.5f * TILE_SIZE) + 6;
	screen_y = camy + (13.25 * TILE_SIZE) - 2;
	for (i = 100, c = 99; i >= 0; i--) { //hpheroe
		k = ((turb) / c);
		if (k && (turb >= 0)) yo = 1.0f;
		else yo = 0.5f;
		if (turb == 100) xo = 0.0f;
		else xo = 0.5f;
		xf = xo + 0.5f;
		yf = yo - 0.5f;
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 8);
		glBegin(GL_QUADS);
		glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
		glTexCoord2f(xf, yo);	glVertex2i(screen_x + 1, screen_y);
		glTexCoord2f(xf, yf);	glVertex2i(screen_x + 1, screen_y + 12);
		glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + 12);
		glEnd();
		glDisable(GL_TEXTURE_2D);
		c -= 1;
		if (c <= 0) c = 1;
		screen_x -= 1;
	}

	//draw throw
	screen_x = camx + SCENE_Xo + (10.5*TILE_SIZE) + 2;
	screen_y = camy + ((13.5) * TILE_SIZE) - 2;
	int kkk = (ctdib / 10);
	ctdib += 1;
	if (ctdib >= 40) ctdib = 0;
	xo = 0.0f; xf = xo + 0.25;
	yo = 0.9375f - (0.25f * (kkk));
	yf = yo - 0.1875;
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 3);
	glBegin(GL_QUADS);
	glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
	glTexCoord2f(xf, yo);	glVertex2i(screen_x + 32, screen_y);
	glTexCoord2f(xf, yf);	glVertex2i(screen_x + 32, screen_y + 24);
	glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + 24);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	//draw supalatigo
	screen_x = camx + SCENE_Xo + (9*TILE_SIZE) + 2;
	screen_y = camy + ((13.25f) * TILE_SIZE) - 2;
	if (supalatigo){
		xo = 0.0f; xf = xo + 0.1f;
		yo = 0.6f; yf = yo - 0.1f;
	}
	else {
		xo = 0.2f; xf = xo + 0.1f;
		yo = 0.6f; yf = yo - 0.1f;
	}
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 10);
	glBegin(GL_QUADS);
	glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
	glTexCoord2f(xf, yo);	glVertex2i(screen_x + 32, screen_y);
	glTexCoord2f(xf, yf);	glVertex2i(screen_x + 32, screen_y + 32);
	glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + 32);
	glEnd();
	glDisable(GL_TEXTURE_2D);


	//draw llave cofre
	if (llave == 1 || llave == 3){
		screen_x = camx + SCENE_Xo + (12*TILE_SIZE);
		screen_y = camy + ((13) * TILE_SIZE) + 6;
		xo = 0.0f; xf = xo + 0.1f;
		yo = 0.2f; yf = yo - 0.1f;
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 10);
		glBegin(GL_QUADS);
		glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
		glTexCoord2f(xf, yo);	glVertex2i(screen_x + 32, screen_y);
		glTexCoord2f(xf, yf);	glVertex2i(screen_x + 32, screen_y + 32);
		glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + 32);
		glEnd();
		glDisable(GL_TEXTURE_2D);
	}

	//draw llave puerta
	if (llave == 2 || llave == 3){
		screen_x = camx + SCENE_Xo + (13*TILE_SIZE);
		screen_y = camy + ((13) * TILE_SIZE) + 6;
		xo = 0.0f; xf = xo + 0.1f;
		yo = 0.1f; yf = yo - 0.1f;
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 10);
		glBegin(GL_QUADS);
		glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
		glTexCoord2f(xf, yo);	glVertex2i(screen_x + 32, screen_y);
		glTexCoord2f(xf, yf);	glVertex2i(screen_x + 32, screen_y + 32);
		glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + 32);
		glEnd();
		glDisable(GL_TEXTURE_2D);
	}

	//draw numbers throw
	screen_x = camx + SCENE_Xo + (10.5*TILE_SIZE) + 2;
	screen_y = camy + ((13.5) * TILE_SIZE) - 2;
	draw_number(screen_x + 8, screen_y - 8, 8, cant_throw/10);
	draw_number(screen_x + 16, screen_y - 8, 8, cant_throw%10);
	//draw numbers points
	screen_x = camx + SCENE_Xo + (5*TILE_SIZE);
	screen_y = camy + (15 * TILE_SIZE) - 20;
	draw_number(screen_x + 16, screen_y, 16, points / 1000);
	draw_number(screen_x + 32, screen_y, 16, points / 100);
	draw_number(screen_x + 48, screen_y, 16, points / 10);
	draw_number(screen_x + 64, screen_y, 16, points % 10);
	//draw number lives
	screen_x = camx + SCENE_Xo + (13 * TILE_SIZE);
	draw_number(screen_x + 16, screen_y, 16, lives);
	//draw number stage
	screen_x = camx + SCENE_Xo + (10.5 * TILE_SIZE);
	draw_number(screen_x + 16, screen_y, 16, stage);
}

void cScene::DrawGuarro(int tex_id, float xo, float yo, float xf, float yf, float scx, float scy, int ancho, int largo){
	int screen_x, screen_y;
	screen_x = scx;
	screen_y = scy;

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, tex_id);
	glBegin(GL_QUADS);
	glTexCoord2f(xo, yo);	glVertex2i(screen_x, screen_y);
	glTexCoord2f(xf, yo);	glVertex2i(screen_x + ancho, screen_y);
	glTexCoord2f(xf, yf);	glVertex2i(screen_x + ancho, screen_y + largo);
	glTexCoord2f(xo, yf);	glVertex2i(screen_x, screen_y + largo);
	glEnd();

	glDisable(GL_TEXTURE_2D);
}

