#pragma once

#include "cBicho.h"

#define PLAYER_START_CX		32
#define PLAYER_START_CY		32

class cPlayer: public cBicho
{
public:
	cPlayer();
	~cPlayer();

	void Draw(int tex_id);
};
