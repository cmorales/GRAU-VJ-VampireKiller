#pragma once

#include "cTexture.h"
#include "Globals.h"

#define FRAME_DELAY		6
#define STEP_LENGTH		2
#define JUMP_HEIGHT		80
#define JUMP_STEP		4
#define delay_subir		160
#define delay_throw		100
#define delay_attack	100
#define da�o_latigo		3
#define da�o_energy		2

#define STATE_LOOKLEFT						0
#define STATE_LOOKRIGHT						1
#define STATE_WALKLEFT						2
#define STATE_WALKRIGHT						3
#define STATE_JUMPLEFT						5
#define STATE_JUMPRIGHT						6
#define STATE_ESCALERALEFT					7
#define STATE_ESCALERARIGHT					8
#define STATE_ESCALERALEFTBOT				9
#define STATE_ESCALERARIGHTBOT				10
#define STATE_WAITINGESCALERARIGHT			11
#define STATE_WAITINGESCALERALEFT			12
#define STATE_WAITINGESCALERALEFTBOT		13
#define STATE_WAITINGESCALERARIGHTBOT		14
#define STATE_THROWRIGHT					15
#define STATE_THROWLEFT						16
#define STATE_ATTACKRIGHT					17
#define STATE_ATTACKLEFT					18
#define STATE_CROUCHLEFT					19
#define STATE_CROUCHRIGHT					20
#define STATE_ATTACKCROUCHRIGHT				21
#define STATE_ATTACKCROUCHLEFT				22
#define STATE_ATTACKESCALERARIGHT			23
#define STATE_ATTACKESCALERALEFT			24
#define STATE_ATTACKESCALERARIGHTBOT		25
#define STATE_ATTACKESCALERALEFTBOT			26
#define STATE_DEAD							27
#define STATE_PAUSE							28
#define STATE_END_LEVEL						29

class cRect
{
public:
	int left,top,
		right,bottom;
};

class cBicho
{
public:
	cBicho(void);
	cBicho(int x,int y,int w,int h);
	~cBicho(void);

	void SetPosition(int x,int y);
	bool FinEscalera(int *map);
	void GetPosition(int *x,int *y);
	void SetTile(int tx,int ty);
	void GetTile(int *tx,int *ty);
	void SetWidthHeight(int w,int h);
	void GetWidthHeight(int *w,int *h);

	bool Collides(cRect *rc);
	bool CollidesMapEscaleraBot(int *map, bool right);
	bool CollidesMapEscalera(int *map, bool right);
	bool CollidesMapWall(int *map,bool right);
	bool CollidesMapFloor(int *map);
	void GetArea(cRect *rc);
	void DrawRect(int tex_id,float xo,float yo,float xf,float yf);

	void MoveRight(int *map);
	void MoveLeft(int *map);
	void MoveEscalera(int *map, bool right);
	void Jump(int *map);
	void Stop();
	void Logic(int *map);
	bool Throw(bool right);
	void Attack(bool right);
	void SetSubiendo(bool b);
	int GetDa�oLatigo();
	int GetDa�oEnergy();
	void SetJumping(bool a);

	int  GetState();
	bool GetJump();
	bool GetEscalera();
	int GetLlaves();
	void SetLlaves(int l);
	void SetState(int s);
	int SubiendoAct();
	int ThrowAct();
	int AttackAct();
	void SetThrowAct(int a);
	void SetAttackAct(int a);
	int GetLives();
	void SetLives(int l);
	bool GetSupalatigo();
	void SetSupalatigo(bool v);
	void SetTurbo(float f);
	float GetTurbo();

	void NextFrame(int max);
	int GetFrame();
	int GetDamaged();
	void SetDamaged(int a);
	int GetHp();
	void SetHp(int a);
	int GetThrowa();
	void SetThrowa(int a);
	int DeadAct();
	void SetDeadAct(int s);
	
private:
	int x,y;
	int w,h;
	int state;
	int hp;
	int lives;
	int cantidad_throw;
	int damaged;
	int llaves; //0sin, 1cofre, 2puerta, 3ambas

	bool jumping;
	bool subiendo;
	int subiendo_act;
	int attack_act;
	int throw_act;
	int dead_act;
	int jump_alfa;
	int jump_y;
	bool supalatigo;
	float turbo;

	int seq,delay;
	//int escalera_actual; //1(subir drx), 2(subir izk), 3(bajar drx), 4(bajar izk)
};
