
#include "Globals.h"
#include "cGame.h"

//Delete console
#pragma comment(linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"")

#define FRAMERATE 60

cGame Game;

int initTime;
int fps_time = 0;
int framess = 0;

void AppRender()
{
	Game.Render();
}
void AppKeyboard(unsigned char key, int x, int y)
{
	Game.ReadKeyboard(key,x,y,true);
}
void AppKeyboardUp(unsigned char key, int x, int y)
{
	Game.ReadKeyboard(key,x,y,false);
}
void AppSpecialKeys(int key, int x, int y)
{
	Game.ReadKeyboard(key,x,y,true);
}
void AppSpecialKeysUp(int key, int x, int y)
{
	Game.ReadKeyboard(key,x,y,false);
}
void AppMouse(int button, int state, int x, int y)
{
	Game.ReadMouse(button,state,x,y);
}

int cc = 0;
void AppIdle()
{
	if (glutGet(GLUT_ELAPSED_TIME) > (fps_time + 1000)) {
		framess = cc;
		cc = 0;
		fps_time = glutGet(GLUT_ELAPSED_TIME);
	}
	cc++;
	if(glutGet(GLUT_ELAPSED_TIME) - initTime > 1000.f/FRAMERATE){
		initTime = glutGet(GLUT_ELAPSED_TIME);
		if(!Game.Loop(framess)) exit(0);
	}
}

void main(int argc, char** argv)
{
	int res_x,res_y,pos_x,pos_y;

	//GLUT initialization
	glutInit(&argc, argv);

	//RGBA with double buffer
	glutInitDisplayMode(GLUT_RGBA | GLUT_ALPHA | GLUT_DOUBLE);

	//Create centered window
	res_x = glutGet(GLUT_SCREEN_WIDTH);
	res_y = glutGet(GLUT_SCREEN_HEIGHT);
	pos_x = (res_x>>1)-(GAME_WIDTH>>1);
	pos_y = (res_y>>1)-(GAME_HEIGHT>>1);
	
	//glutInitWindowPosition(pos_x,pos_y);
	//glutInitWindowSize(GAME_WIDTH,GAME_HEIGHT);
	//glutCreateWindow("Bubble returns!");

	//glutGameModeString("1366x768:32");
	glutEnterGameMode();

	//Make the default cursor disappear
	glutSetCursor(GLUT_CURSOR_NONE);

	//Register callback functions
	glutDisplayFunc(AppRender);			
	glutKeyboardFunc(AppKeyboard);		
	glutKeyboardUpFunc(AppKeyboardUp);	
	glutSpecialFunc(AppSpecialKeys);	
	glutSpecialUpFunc(AppSpecialKeysUp);
	glutMouseFunc(AppMouse);
	glutIdleFunc(AppIdle);

	//Game initializations
	Game.Init();

	initTime = glutGet(GLUT_ELAPSED_TIME);


	//Application loop
	
	glutMainLoop();	
}
