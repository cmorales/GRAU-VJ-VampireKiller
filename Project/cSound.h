#pragma once

#pragma comment(lib, "fmodex_vc.lib" ) // fmod library

#include "fmod.hpp" //fmod c++ header

//Sound array size
#define NUM_SOUNDS 32

//Sound identifiers
#define SOUND_MENU			0
#define	SOUND_AMBIENT1      1
#define	SOUND_AMBIENT2      2

#define SOUND_LATIGO		3
#define SOUND_RHP			4
#define SOUND_RVIDA			5
#define SOUND_RMUNI			6	
#define SOUND_COFRE			7	
#define SOUND_CAER			8
#define SOUND_DMG			9
#define SOUND_ENTRY			10
#define SOUND_SELECT		11
#define SOUND_PAUSE			12
#define SOUND_MUERTE		13
#define SOUND_THROW			14
#define SOUND_POWER			15
#define SOUND_IRON			16
#define SOUND_BOLA			17
#define SOUND_KAME			18
#define SOUND_KILL			19
#define SOUND_FAIL			20
#define SOUND_PETAR			21

#define SOUND_STAGE			22





class cSound
{
public:
	cSound(void);
	virtual ~cSound(void);

	bool Load();
	void Play(int sound_id);
	void Stop(int sound_id);
	void SmoothStop(int sound_id);
	void StopAll();
	void Update();

	FMOD::System*     system; //handle to FMOD engine
	FMOD::Sound*      sounds[NUM_SOUNDS]; //sound that will be loaded and played
	FMOD::Channel*    ambient1Channel;
	FMOD::Channel*    ambient2Channel;
	FMOD::Channel*    menuChannel;
	FMOD::DSP*        dspSmoothStop;
};