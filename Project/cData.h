#pragma once

#include "cTexture.h"
#include "Globals.h"

//Image array size
#define NUM_IMG					16

//Image identifiers
#define IMG_BLOCKS				0
#define IMG_PLAYER				1
#define IMG_CRUZ				2
#define IMG_IRONMAN				3
#define IMG_GOHAN				4
#define IMG_CARA				5
#define IMG_JESUCRISTO			6		
#define IMG_HPBAR				7
#define iMG_NUMEROS				8
#define IMG_OBJETOS				9
#define IMG_MAINMENU			10
#define IMG_CREDITOS			11
#define IMG_INSTRUCCIONES		12
#define IMG_ZUBAT				13
#define IMG_DEAD				14
#define IMG_PAUSE				15

//#define IMG_SHOOT	5




class cData
{
public:
	cData(void);
	~cData(void);

	int  GetID(int img);
	void GetSize(int img,int *w,int *h);
	bool LoadImage(int img,char *filename,int type = GL_RGBA);

private:
	cTexture texture[NUM_IMG];
};
